function rodarPrograma() {

  // obtém elementos da tela para trabalhar com eles
  const $dadosPokemon = document.getElementById( 'dadosPokemon' )
  const $h1 = $dadosPokemon.querySelector( 'h1' )
  const $img = $dadosPokemon.querySelector( '#thumb' )
  const $txtIdPokemon = document.getElementById( 'txtNumero' )
  // instanciando objeto de API que criamos para facilitar a comunicação
  const url = 'https://pokeapi.co/api/v2/pokemon'
  const pokeApi = new PokeApi( url )

  // registrar evento de onblur
  $txtIdPokemon.onblur = function() {
    const idDigitado = $txtIdPokemon.value
    pokeApi.buscar( idDigitado )
      .then( res => res.json() )
      .then( dadosJson => {
        // criar objeto pokémon e renderiza-lo na tela
        const pokemon = new Pokemon( dadosJson )
        renderizarPokemonNaTela( pokemon )
      } )
  }

  function renderizarPokemonNaTela( pokemon ) {
    $h1.innerText = pokemon.nome
    $img.src = pokemon.thumbUrl
  }

}

rodarPrograma()
