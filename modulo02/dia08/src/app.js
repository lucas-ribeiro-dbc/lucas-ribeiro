const urlApi = 'https://pokeapi.co/api/v2/pokemon'
const pokeApi = new PokeApi( urlApi )

let app = new Vue( {
  el: '#dadosPokemon',
  data: {
    idParaBuscar: '',
    pokemon: { }
  },
  methods: {
    async buscar() {
      const numeroPokemon = document.getElementById("numeroPokemon")
      if(!numeroPokemon.checkValidity()){
        alert("Nao pode ser digitado letras, apenas numeros!")
      }
      else if(localStorage.idPokemon !== this.idParaBuscar){ 
        this.pokemon = await pokeApi.buscar( this.idParaBuscar )
        localStorage.idPokemon = this.idParaBuscar
      }
    },
    async buscarComSorte(){
      var numero = Math.floor(Math.random() * (802 - 1) + 1 )
      if(localStorage.getItem(numero) !== "true"){
        localStorage.setItem(`${numero}`, "true")
        this.pokemon = await pokeApi.buscar( numero  )
      }
    }
  }
} )
