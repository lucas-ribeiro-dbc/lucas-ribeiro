//Escopo global
banana = 'Banana'
var maracuja = 'Passion Fruit'
function escolherFruta(){
  //Escopo local
  var maracuja = 'Maracujá'
}
//IIFE
!(function(){
  var maracuja = 'Maracujina'
  console.log(maracuja);
})()
