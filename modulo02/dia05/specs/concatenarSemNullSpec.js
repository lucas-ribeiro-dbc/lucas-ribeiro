describe( 'concatenarSemNull', function() {
  const expect = chai.expect
  beforeEach( function() {
    chai.should()
  } )

  it( 'passar null primeiro parâmetro', function() {
    concatenarSemNull( null, 'Soja' ).should.equal( 'Soja' )
  } )

  it( 'passar null segundo parâmetro', function() {
    concatenarSemNull( 'Soja é', null ).should.equal( 'Soja é' )
  } )

  it( 'passar texto nos dois parâmetros', function() {
    concatenarSemNull( 'Soja', ' é bom' ).should.equal( 'Soja é bom' )
  } )

  it( 'passar undefined no segundo parâmetro', function() {
    concatenarSemNull( 'Soja é' ).should.equal( 'Soja éundefined' )
  } )

  it( 'passar null nos dois parâmetros', function() {
    concatenarSemNull( null, null ).should.equal( '' )
  } )
} )
