//Exercicio 01
console.log("---------Exercicio 01 ---------");

var circulo1 = {
  raio: 5,
  tipoCirculo: "A"
}
var circulo2 = {
  raio: 5,
  tipoCirculo: "B"
}
var circulo3 = {
  raio: 5,
  tipoCirculo: "C"
}
function calcularCirculo(circulo) {
  if(circulo.tipoCirculo === "A"){
    return Math.PI * Math.pow(circulo.raio, 2)
  }
  else if(circulo.tipoCirculo ==="C"){
    return Math.PI * 2 * circulo.raio
  }
 }

 console.log(calcularCirculo(circulo1))
 console.log(calcularCirculo(circulo2))
 console.log(calcularCirculo(circulo3))


 //Exercicio 02
 console.log("---------Exercicio 02 ---------");
 function naoBissexto(ano) { 
   if(typeof ano === "number" && ano%400 ==0 || typeof ano === "number" && ano%4==0 && ano%100 !=0){
     return false
   }
   return true
  }

  console.log(naoBissexto(2016))
  console.log(naoBissexto("a"))
  console.log(naoBissexto(2017))

  //Exercicio 03
  console.log("---------Exercicio 03 ---------");
function concatenarSemUndefined(x,y){
  if(typeof x === "undefined" && typeof y === "undefined"){
    return ""
  }
  else if(typeof x ==="undefined"){ 
    return "" + y
  }
  else if(typeof y ==="undefined"){ 
    return x + ""
  }else{
    return x + " " + y;
  }
}

console.log(concatenarSemUndefined( undefined, "Soja" )) // “Soja”
console.log(concatenarSemUndefined( "Soja", "é bom" )) // “Soja é bom”
console.log(concatenarSemUndefined( "Soja é" )) // “Soja é”

//Exercicio04
console.log("---------Exercicio 04 ---------");
function concatenarSemNull(x,y){
  if(x === null && y === null){
    return ""
  }
  else if(x === null){ 
    return "" + y
  }
  else if(y === null){ 
    return x + ""
  }else{
    return x + " " + y;
  }
}

console.log(concatenarSemNull( null, "Soja" )) // “Soja”
console.log(concatenarSemNull( "Soja", "é bom" )) // “Soja é bom”
console.log(concatenarSemNull( "Soja é")) // “Soja é undefined”

//Exercicio05
console.log("---------Exercicio 05 ---------");
function concatenarEmPaz(x,y){
  if(x === null && typeof y === null 
    || typeof x === "undefined" && typeof y === "undefined" 
    || typeof x === null && typeof y === "undefined"
    || typeof x === "undefined" && typeof y === null){
    return ""
  }
  else if(x === null || typeof x === "undefined"){ 
    return "" + y
  }
  else if(y === null || typeof y === "undefined"){ 
    return x + ""
  }else{
    return x + " " + y;
  }
}

console.log(concatenarEmPaz( undefined, "Soja" ))// “Soja”
console.log(concatenarEmPaz( "Soja é bom, mas...", null )) // “Soja é bom, mas...”
console.log(concatenarEmPaz( "Soja é")) // “Soja é”

//Exercicio06
console.log("---------Exercicio 06 ---------");
function adicionar(x){
  return y => {
    return x+y
  }
  }

console.log(adicionar(3)(4)) // 7
console.log(adicionar(5642)(8749)) // 14391




//Exercicio07
console.log("---------Exercicio 07 ---------");
function fiboSum(numero) {  
  if (numero <= 1) return 1
  return fiboSum(numero - 1) + fiboSum(numero - 2)
}
function fibonacci(numero) {
  var resultado = 0;
  for(var i = 0 ; i < numero; i++){
    resultado += fiboSum(i);
  }
  return resultado;
  }
console.log(fibonacci(7));

