function rodarPrograma() {
  const $dadosPokemon = document.getElementById("dadosPokemon")
  const $nomePokemon = document.getElementById("nomePokemon")
  const $imgPokemon = document.getElementById("imgPokemon")
  const $alturaPokemon = document.getElementById("alturaPokemon")
  const $pesoPokemon = document.getElementById("pesoPokemon")
  const $tiposPokemon = document.getElementById("tiposPokemon")
  const $estatisticasPokemon = document.getElementById("estatisticasPokemon")
  const $numeroPokemon = document.getElementById("numeroPokemon")
  const $btnSorte = document.getElementById("btnSorte")
  const $idPokemon = document.getElementById("idPokemon")
  
  $btnSorte.onclick = function(){
    var numero = numeroAleatorio(1,802)
    if(localStorage.getItem(numero) !== "true"){
      localStorage.setItem(`${numero}`, "true")
      fazerRequisicao(numero)
    }
  }
  function numeroAleatorio(min, max) {
    var numero = Math.random() * (max - min) + min;
    return Math.floor(numero)
  }
  
  $numeroPokemon.onblur = function(){
    if(!$numeroPokemon.checkValidity()){
      alert("Não pode ser digitado letras, apenas números!")
    }
    else if(localStorage.idPokemon !== $numeroPokemon.value){ 
      fazerRequisicao($numeroPokemon.value)
    }
  }
  function fazerRequisicao(numeroPokemon) { 
    $tiposPokemon.innerHTML = ''
    $estatisticasPokemon.innerHTML = ''
    let url = `https://pokeapi.co/api/v2/pokemon/${numeroPokemon}/`
    localStorage.idPokemon = $numeroPokemon.value
    let requisicao = fetch(url)
    //Transforma a string recebida pela promise em um objeto json que possa ser lido/percorrido pelo javascript
    .then(res => res.json())
    .then(dadosJson => {
      var pokemon = new Pokemon(numeroPokemon, dadosJson)
      $nomePokemon.innerText = pokemon.nome
      $imgPokemon.src = pokemon.img
      $imgPokemon.id = "img-poke"
      $idPokemon.innerText = `ID: ${pokemon.id}`
      $alturaPokemon.innerText = `Altura: ${pokemon.altura}cm`
      $pesoPokemon.innerText = `Peso: ${pokemon.peso}kg`
      var h1Tipo = document.createElement('h1')
      h1Tipo.innerText = "TIPOS"
      h1Tipo.id = "tipos"
      $tiposPokemon.appendChild(h1Tipo)

      for(var i = 0 ; i < pokemon.tipos.length;i++){
        var li = document.createElement('li')
        li.id = "li-tipo"
        li.innerText = pokemon.tipos[i].type.name.toUpperCase()
        $tiposPokemon.appendChild(li)
      }
      var h1Estatistica = document.createElement('h1')
      h1Estatistica.innerText = "ESTATÍSTICAS"
      h1Estatistica.id = "estatisticas"
      $estatisticasPokemon.appendChild(h1Estatistica)
      for(var i = 0 ; i < pokemon.estatisticas.length;i++){
        var ul = document.createElement('ul')
        var stat_name = document.createElement('h2')
        stat_name.innerText = pokemon.estatisticas[i].stat.name.toUpperCase()
        var li_base_stat = document.createElement('li')
        var li_effort = document.createElement('li')
        li_base_stat.innerText = `Base Stat: ${pokemon.estatisticas[i].base_stat}`
        li_effort.innerText = `Effort: ${pokemon.estatisticas[i].effort}`
        ul.appendChild(stat_name)
        ul.appendChild(li_effort)
        ul.appendChild(li_base_stat)
        $estatisticasPokemon.appendChild(ul)
      }
    })
  }
}
rodarPrograma()
