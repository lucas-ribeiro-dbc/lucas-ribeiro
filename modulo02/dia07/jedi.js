class Jedi{
  constructor(nome){
    this.nome = nome
    this.h1 = document.createElement('h1')
    this.h1.innerText = this.nome
    this.h1.id = `jedi_${this.nome}`
    const dadosJedi = document.getElementById('dadosJedi')
    dadosJedi.appendChild(this.h1)
  }
   // UTILIZANDO SELF
  // atacar(){
  //   let self = this
  //   setTimeout(function(){
  //     selfie.h1.innerText += ' atacou! '
  //   }, 1000)
  // }

  // UTILIZANDO BIND
  // atacar(){
  //   setTimeout(function(){
  //     selfie.h1.innerText += ' atacou! '
  //   }.bind(this), 1000)
  // }

   // UTILIZANDO ARROWFUNCTION
  atacar(){
    setTimeout(() =>{
      selfie.h1.innerText += ' atacou! '
    }, 1000)
  }
}
