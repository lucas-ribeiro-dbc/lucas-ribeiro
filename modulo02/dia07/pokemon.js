class Pokemon{
  constructor(id, jsonVindoDaApi){
    this._id = id
    this._nome = jsonVindoDaApi.name.toUpperCase()
    this._img = jsonVindoDaApi.sprites.front_default
    this._altura = parseInt(jsonVindoDaApi.height)*10
    this._peso = parseInt(jsonVindoDaApi.weight)/10
    this._tipos = jsonVindoDaApi.types
    this._estatisticas = jsonVindoDaApi.stats
  }
  get id(){
    return this._id
  }
  get nome(){
    return this._nome
  }
  get img(){
    return this._img
  }
  get altura(){
    return this._altura
  }
  get peso(){
    return this._peso
  }
  get tipos(){
    return this._tipos
  }
  get estatisticas(){
    return this._estatisticas
  }
}
