export default class ValidatorEmail{
constructor(){
}
pegaValidator(){
  const validatorDbc = {
    getMessage(field, args) {
      return "O email deve ser do domínio @dbccompany.com.br"
    },
    validate(value, args) {
      const dominio = value.split('@');
      return dominio[1] === "dbccompany.com.br"
    }
}
  return validatorDbc
}
}
