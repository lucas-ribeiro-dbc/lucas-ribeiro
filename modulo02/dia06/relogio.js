class Relogio{
  constructor(){
    this.idIntervalo
    this.btnReiniciar = document.getElementById('btnReiniciar')
    this.iniciarRelogio()
  }
  iniciarRelogio(){
  const h1 = document.getElementById('horario')
  const idInterval = setInterval(function() {
    h1.innerText = new Date().toLocaleTimeString() 
   }, 1000)
   this.btnReiniciar.disabled = true
   this.idIntervalo = idInterval
  }
  pararRelogio(){
    clearInterval(this.idIntervalo)
    this.btnReiniciar.disabled = false
  }
  reiniciarRelogio(){
    this.iniciarRelogio()
  }

}

