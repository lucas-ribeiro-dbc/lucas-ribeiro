// PROTOTYPE
// function Elfo(nome){
//   this.nome = nome
// }
// Elfo.prototype.atirarFecha =  function(){
//   console.log("Atirar flecha");  
// }
//Vai dar erro pois a classe Elfo ainda nao foi criada
// const galadriel = new Elfo('Galadriel')
class Personagem{
  constructor(nome){
    this._nome = nome
  }
    get nome(){
      return this._nome
    }
    set nome(nome){
      document.getElementById('nome').innerText = nome
      this._nome = nome
    }
  }
class Elfo extends Personagem {
  constructor(nome){
    super(nome)
  }
  static qtdElfos(){
    return 2
  }
  atirarFecha(){
    console.log("Atirar flecha"); 
  }
}
const legolas = new Elfo ('Legolas')
legolas.nome = "L"
console.log(legolas.nome);
legolas.atirarFecha()

const feanor = new Elfo ('Feanor')
feanor.nome = "F"
console.log(feanor.nome);
legolas.atirarFecha()
console.log(Elfo.qtdElfos());

//Diferente de
// const legolas = {
//   nome: legolas,
//   atirarFlecha = function () {}
// }
