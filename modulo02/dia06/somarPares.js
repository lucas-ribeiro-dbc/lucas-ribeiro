// não pode ser chamada antes da sua invocação / execução
// const somarPares = function( numeros ) {
// }

// const somarPares = numeros => {
//   var i
//   i += 2
//   return i
// }

// sofre hoisting
// é hasteada para o início do script e pode ser chamada "antes"
function somarPares() {
  var numeros = arguments[ 0 ]
  
  console.log(soma)
  // por causa do hasteamento, não dá erro, pois fica acima do console.log
  var soma = 0

  //numeros.forEach( ( elemento, indice ) => console.log( elemento, indice ) )
  numeros.forEach( ( elemento, indice ) => {
    // if ( indice % 2 === 0 ) {
    //   soma += elemento
    // }
    soma += indice % 2 === 0 ? elemento : 0
  } )

  return soma
}


// function forEach( funcao ) {
//   for ( var i = 0; i < this.elementos.length; i++ ) {
//     funcao( this.elementos[ i ], i, this.elementos )
//   }
// }






























