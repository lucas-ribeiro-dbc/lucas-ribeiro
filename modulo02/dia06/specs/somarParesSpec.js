describe( 'somarPares', function() {
  beforeEach( function() {
    chai.should()
  } )

  
  it ( '[ 1, 2 ] deve somar apenas 1', function() {
    somarPares( [ 1, 2 ] ).should.equal( 1 )
  } )

  it ( '[ 1, 1, 1, 1 ] deve somar apenas 1(0) e 1(2)', function() {
    somarPares( [ 1, 1, 1, 1 ] ).should.equal( 2 )
  } )

  it ( '[] deve retornar 0', function() {
    somarPares( [ ] ).should.equal( 0 )
  } )
} )
