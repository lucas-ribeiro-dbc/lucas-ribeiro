const meuH2 = document.getElementById( 'tituloPagina' )

function perguntarNome() {
  const nome = prompt( 'Qual seu nome?' )
  meuH2.innerText = nome
  localStorage.nome = nome
}

function renderizaNomeArmazenadoNaTela() {
  meuH2.innerText = localStorage.nome
}

function rodarPrograma() {
  const nomeArmazenado = localStorage.nome && localStorage.nome.length > 0
  if ( nomeArmazenado ) {
    renderizaNomeArmazenadoNaTela()
  } else {
    perguntarNome()
  }
  if ( !localStorage.qtdCliques ) {
    localStorage.qtdCliques = 0
  }
  const btnLimparDados = document.getElementById( 'btnLimparDados' )
  const temQueDesabilitar = localStorage.qtdCliques >= 5
  if ( temQueDesabilitar ) {
    btnLimparDados.disabled = true
  }

  btnLimparDados.onclick = function() {
    let armazenamentoLocal = localStorage
    armazenamentoLocal.removeItem( 'nome' )
    localStorage.qtdCliques++
    if ( localStorage.qtdCliques >= 5 ) {
      btnLimparDados.disabled = true
    }
  }
}

rodarPrograma()




// OWASP
// CSRF
// XSS
// SQL Injection




// WebDriverJS, Selenium, Protractor
// var pagina = abreONavegador.entraNaUrl('www.dbccompany.com.br')
// pagina.clicarNoBotao( 'idDoBotao' )
// pagina.preencherTextoNoInput( '' )


//meuH2.innerText = 'Carregando...'

//localStorage['nome'] = nome
//localStorage.setItem( 'nome', nome )


// TODO:
// adicionar contador para, após quinto click, desabilitar botao
// verificar, no carregamento da página, se tem que desabilitar o botão ou não
