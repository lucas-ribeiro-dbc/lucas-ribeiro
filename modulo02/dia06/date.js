function main(){
  const h1 = document.getElementById('horario')
  const btnReiniciar = document.getElementById('btnReiniciar')
  const btnPararRelogio = document.getElementById('btnPararRelogio')

  const relogio = new Relogio()

  btnPararRelogio.onclick = function(){
    relogio.pararRelogio()
  }

  btnReiniciar.onclick = function() {
    relogio.reiniciarRelogio()
  } 
}
main()
