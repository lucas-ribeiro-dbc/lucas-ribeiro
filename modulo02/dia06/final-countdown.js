function main(){
  const contador = document.getElementById('contador')
  const circulo = document.getElementById('circle')
  const imgTriste = document.getElementById('imgTriste')
  contador.innerText = 30
  const idInterval = setInterval(function(){
      var valor = parseInt(contador.innerText)
      if(valor === 0){
        clearInterval(idInterval)
        circulo.className += "tempoEncerrado"
        contador.innerText = "Tempo encerrado!"
        imgTriste.className+= "imgInline"
      }
      else{
        valor--
        console.log(valor);
        contador.innerText = valor.toString()
        
      }

  }, 1000)
}
main()
