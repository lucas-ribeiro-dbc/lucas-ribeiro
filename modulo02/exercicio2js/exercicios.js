function somarPosicaoPares(array){
  var resultado = 0
  for (let index = 0; index < array.length; index+=2) {
  resultado += parseFloat(array[index])
  }
  return resultado;
}

function formatarElfos(array) { 
  array.forEach(elfo =>{
    elfo.nome = elfo.nome.toUpperCase()
    elfo.temExperiencia = elfo.experiencia > 0 ? true : false
    elfo.descricao = elfo.temExperiencia === true ? `${elfo.nome} com experiencia e com ${elfo.qtdFlechas} flecha(s)` 
    : `${elfo.nome} sem experiencia e com ${elfo.qtdFlechas} flecha(s)`
    delete elfo.experiencia
  })
  return array
 }
