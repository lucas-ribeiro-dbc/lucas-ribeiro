describe( 'somaPosicaoPares', function() {

  // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
  // documentação: https://www.chaijs.com/
  beforeEach( function() {
    chai.should()
  } )

  // cada bloco "it" é um cenário de teste, como se fosse um @Test do JUnit
  // importante: garanta que o arquivo onde o código está implementado foi incluído, em rodar-testes.html
  it( 'resultado deve ser 3.34', function() {
    const resultado = somarPosicaoPares( [ 1, 56, 4.34, 6, -2 ]  )
    resultado.should.equal( 3.34 )
  } )

  it( 'resultado deve ser 10', function() {
    const resultado = somarPosicaoPares( [ 10, 56, 25, 6]  )
    resultado.should.equal( 35 )
  } )

  it( 'resultado deve ser 0', function() {
    const resultado = somarPosicaoPares( [ ]  )
    resultado.should.equal( 0 )
  } )
} )
