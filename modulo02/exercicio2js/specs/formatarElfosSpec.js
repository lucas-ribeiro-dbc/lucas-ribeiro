describe( 'formatarElfos', function() {

  // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
  // documentação: https://www.chaijs.com/
  beforeEach( function() {
    chai.should()
  } )

  // cada bloco "it" é um cenário de teste, como se fosse um @Test do JUnit
  // importante: garanta que o arquivo onde o código está implementado foi incluído, em rodar-testes.html
  it( 'elfos com campos alterados', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
    const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo1, elfo2 ]

    const elfo3 = { nome: "LEGOLAS", qtdFlechas: 6, temExperiencia: false, descricao: "LEGOLAS sem experiencia e com 6 flecha(s)" }
    const elfo4 = { nome: "GALADRIEL", qtdFlechas: 1, temExperiencia: true, descricao: "GALADRIEL com experiencia e com 1 flecha(s)" }
    const arrayElfos2 = [elfo3, elfo4]

    const resultado = JSON.stringify(formatarElfos(arrayElfos)) === JSON.stringify(arrayElfos2)
  
    resultado.should.equal(true)
  } )

  it( 'elfos com campos alterados2', function() {
    const elfo1 = { nome: "Felariel", experiencia: 10, qtdFlechas: 5 }
    const elfo2 = { nome: "Celeborn", experiencia: -1, qtdFlechas: 2 }
    const arrayElfos = [ elfo1, elfo2 ]

    const elfo3 = { nome: "FELARIEL", qtdFlechas: 5, temExperiencia: true, descricao: "FELARIEL com experiencia e com 5 flecha(s)" }
    const elfo4 = { nome: "CELEBORN", qtdFlechas: 2, temExperiencia: false, descricao: "CELEBORN sem experiencia e com 2 flecha(s)" }
    const arrayElfos2 = [elfo3, elfo4]

    const resultado = JSON.stringify(formatarElfos(arrayElfos)) === JSON.stringify(arrayElfos2)
  
    resultado.should.equal(true)
  } )

  it( 'elfos com campos alterados3', function() {
    const elfo1 = { nome: "Haldril", experiencia: 0, qtdFlechas: 0 }
    const elfo2 = { nome: "Mordis", experiencia: -15, qtdFlechas: 15 }
    const arrayElfos = [ elfo1, elfo2 ]

    const elfo3 = { nome: "HALDRIL", qtdFlechas: 0, temExperiencia: false, descricao: "HALDRIL sem experiencia e com 0 flecha(s)" }
    const elfo4 = { nome: "MORDIS", qtdFlechas: 15, temExperiencia: false, descricao: "MORDIS sem experiencia e com 15 flecha(s)" }
    const arrayElfos2 = [elfo3, elfo4]

    const resultado = JSON.stringify(formatarElfos(arrayElfos)) === JSON.stringify(arrayElfos2)
  
    resultado.should.equal(true)
  } )
} )
