/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.service.AluguelService;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author lucas.ribeiro
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private FilmeService filmeService;

    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private MidiaService midiaService;
    
    @Override
    protected AbstractRestController getController() {
        return filmeRestController;
    }

    @Before
    public void beforeTest() {
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void createFilmeTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filme.getCategoria().toString()));

        List<Filme> filmes = filmeRepository.findAll();

        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(filme.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filme.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(filme.getCategoria(), filmes.get(0).getCategoria());

    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void updateFilmeTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        
        Filme f1 = filmeService.create(filme).get();
        
        ArrayList<MidiaDTO> midias2 = new ArrayList<>();
        MidiaDTO m4 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(5))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m5 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(5))
                .valor(new BigDecimal(3.9)).build();
        MidiaDTO m6 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(5))
                .valor(new BigDecimal(4.9)).build();

        midias2.add(m4);
        midias2.add(m5);
        midias2.add(m6);

        FilmeDTO filmeUpdate = FilmeDTO.builder()
                .titulo("Dracula")
                .id(f1.getId())
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias2)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", filmeUpdate.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeUpdate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filme.getCategoria().toString()));
    }

    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void FilmeFindAllTest() throws Exception {

        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        FilmeDTO filme2 = FilmeDTO.builder()
                .titulo("Conde Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.create(filme);
        filmeService.create(filme2);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(2));

        List<Filme> filmes = filmeRepository.findAll();

        Assert.assertEquals(2, filmes.size());
        Assert.assertEquals(filme.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filme.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(filme.getCategoria(), filmes.get(0).getCategoria());

        Assert.assertEquals(filme2.getTitulo(), filmes.get(1).getTitulo());
        Assert.assertEquals(filme2.getLancamento(), filmes.get(1).getLancamento());
        Assert.assertEquals(filme2.getCategoria(), filmes.get(1).getCategoria());
    }

    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void filmeFindByIdTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        Filme f1 = filmeService.create(filme).get();

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/{id}", f1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f1.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f1.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(f1.getCategoria().toString()));

        Filme f2 = filmeRepository.findById(f1.getId()).get();

        Assert.assertEquals(f1.getId(), f2.getId());
        Assert.assertEquals(f1.getTitulo(), f2.getTitulo());
        Assert.assertEquals(f1.getLancamento(), f2.getLancamento());
        Assert.assertEquals(f1.getCategoria(), f2.getCategoria());
    }

    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void countByTipoTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        Filme f1 = filmeService.create(filme).get();
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/count/{id}/DVD", f1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
        
        int quantidade = filmeService.countByTipo(f1.getId(), MidiaType.DVD);

        Assert.assertEquals(10, quantidade);
    }

    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void searchTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();
        midias.add(m1);
        midias.add(m2);
        midias.add(m3);
        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        Filme f1 = filmeService.create(filme).get();
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search?page=0&size=10&sort=id,desc&categoria={categoria}&titulo={titulo}&lancamentoIni=10/10/2018&lancamentoFim=11/10/2018"
                , f1.getCategoria(), f1.getTitulo())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f1.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value(f1.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value(f1.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void findPrecoFilmeTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        
        Filme f1 = filmeService.create(filme).get();
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/precos/{id}", f1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void searchCatalogo() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        Filme salvo = filmeService.create(filme).get();
        
        Cliente c = Cliente.builder()
                .nome("nome")
                .rua("Av benno mentz")
                .bairro("Vila ipiranga")
                .cidade("Porto Alegre")
                .estado("RS")
                .telefone("9999")
                .build();
        
        Cliente c1 = clienteService.save(c);
        //simula uma retirada, pra ver se no catálogo retorna todos que estão disponiveis
        //e as que estão alugadas com suas datas de previsão de retorno
        ArrayList<Long> idMidias = new ArrayList<>();
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.DVD).get(0).getId());
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.VHS).get(0).getId());
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.BLUE_RAY).get(0).getId());
        AluguelDTO aluguel = AluguelDTO.builder().idCliente(c1.getId()).midias(idMidias).build();
        aluguelService.retirada(aluguel);
        
        
         restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
         
         List<CatalogoDTO> catalogos = filmeService.searchCatalogo(filme.getTitulo());
         
         Assert.assertEquals(30, catalogos.size());
    }

}