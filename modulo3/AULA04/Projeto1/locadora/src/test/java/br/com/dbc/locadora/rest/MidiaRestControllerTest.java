/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author lucas.ribeiro
 */
public class MidiaRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private MidiaRestController midiaRestController;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private MidiaService midiaService;

    @Override
    protected AbstractRestController getController() {
        return midiaRestController;
    }

    @Before
    public void beforeTest() {
        midiaRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getService method, of class MidiaRestController.
     */
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void midiaCreateTest() throws Exception {
        Filme filme = Filme.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .build();
        Filme f1 = filmeService.save(filme);
        MidiaDTO midia = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(1))
                .valor(new BigDecimal(1.9)).build();
        midiaService.saveMidia(f1, midia);
        List<Midia> midias = midiaRepository.findAll();
        Assert.assertEquals(1, midias.size());
    }

    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void countByTipoTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        Filme f1 = filmeService.create(filme).get();
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/count/DVD")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());

        int quantidade = midiaService.countByTipo(MidiaType.DVD);

        Assert.assertEquals(10, quantidade);
    }

    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void searchTest() throws Exception {
        Filme filme = Filme.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .build();
        Filme f1 = filmeService.save(filme);
        MidiaDTO midia = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(1))
                .valor(new BigDecimal(1.9)).build();
        midiaService.saveMidia(f1, midia);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/search?page=0&size=10&sort=id,desc&categoria={categoria}&titulo={titulo}&lancamentoIni=10/10/2018&lancamentoFim=11/10/2018",
                 f1.getCategoria(), f1.getTitulo())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midiaType").value(midia.getTipo().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1));        
                        
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void midiaFindByIdTest() throws Exception {
        Filme filme = Filme.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .build();
        Filme f1 = filmeService.save(filme);
        MidiaDTO midia = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(1))
                .valor(new BigDecimal(1.9)).build();
        List<Midia> midias = midiaService.saveMidia(f1, midia);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/{id}", midias.get(0).getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.midiaType").value(midia.getTipo().toString()));

        Midia m1 = midiaRepository.findById(midias.get(0).getId()).get();

        Assert.assertEquals(m1.getId(), midias.get(0).getId());
        Assert.assertEquals(m1.getMidiaType(),midias.get(0).getMidiaType());
    }

}
