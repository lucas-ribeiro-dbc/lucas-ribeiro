/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.AluguelService;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author lucas.ribeiro
 */
public class AluguelRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private AluguelRestController aluguelRestController;

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private AluguelService aluguelService;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private MidiaService midiaService;

    @Override
    protected AbstractRestController getController() {
        return aluguelRestController;
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        aluguelRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void retiradaTest() throws Exception {
        ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        Filme salvo = filmeService.create(filme).get();

        Cliente c = Cliente.builder()
                .nome("nome")
                .rua("Av benno mentz")
                .bairro("Vila ipiranga")
                .cidade("Porto Alegre")
                .estado("RS")
                .telefone("9999")
                .build();

        Cliente c1 = clienteService.save(c);
        //simula uma retirada, pra ver se no catálogo retorna todos que estão disponiveis
        //e as que estão alugadas com suas datas de previsão de retorno
        ArrayList<Long> idMidias = new ArrayList<>();
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.DVD).get(0).getId());
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.VHS).get(0).getId());
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.BLUE_RAY).get(0).getId());
        AluguelDTO aluguel = AluguelDTO.builder().idCliente(c1.getId()).midias(idMidias).build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        List<Aluguel> alugueis = aluguelRepository.findAll();

        Assert.assertEquals(1, alugueis.size());
    }
    
    @Test

    @WithMockUser(username = "admin.admin",

            password = "jwtpass",

            authorities = {"ADMIN_USER"})

    public void devolucaoSemMultaTest() throws Exception {

         ArrayList<MidiaDTO> midias = new ArrayList<>();
        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(new Long(10))
                .valor(new BigDecimal(1.9)).build();
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(new Long(10))
                .valor(new BigDecimal(2.9)).build();
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(new Long(10))
                .valor(new BigDecimal(3.9)).build();

        midias.add(m1);
        midias.add(m2);
        midias.add(m3);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("Dracula")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        Filme salvo = filmeService.create(filme).get();

        Cliente c = Cliente.builder()
                .nome("nome")
                .rua("Av benno mentz")
                .bairro("Vila ipiranga")
                .cidade("Porto Alegre")
                .estado("RS")
                .telefone("9999")
                .build();

        Cliente c1 = clienteService.save(c);
        ArrayList<Long> idMidias = new ArrayList<>();
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.DVD).get(0).getId());
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.VHS).get(0).getId());
        idMidias.add(midiaService.findAllByIdFilmeAndType(salvo.getId(), MidiaType.BLUE_RAY).get(0).getId());
        AluguelDTO aluguel = AluguelDTO.builder().idCliente(c1.getId()).midias(idMidias).build();

        aluguelService.retirada(aluguel);

        AluguelDTO aluguelDevolucao = AluguelDTO.builder().midias(idMidias).build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")

                .contentType(MediaType.APPLICATION_JSON_UTF8)

                .content(objectMapper.writeValueAsBytes(aluguelDevolucao)))

                .andExpect(MockMvcResultMatchers.status().isOk())

                .andExpect(MockMvcResultMatchers.content()

                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

    }

}