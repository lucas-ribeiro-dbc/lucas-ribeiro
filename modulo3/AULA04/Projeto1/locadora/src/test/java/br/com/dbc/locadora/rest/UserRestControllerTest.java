/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.UserRepository;
import br.com.dbc.locadora.service.RoleService;
import br.com.dbc.locadora.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author lucas.ribeiro
 */
public class UserRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private UserRestController userRestController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserRestControllerTest() {
    }

    @Autowired
    protected MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Before
    public void beforeTest() {
        userRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void userCreateTest() throws Exception {
        UserDTO u = UserDTO.builder()
                .firstName("Lucas")
                .lastName("Ribeiro")
                .username("lucas.ribeiro")
                .password(passwordEncoder.encode("1234")).build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(u)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(u.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(u.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(u.getUsername()));
        List<User> users = userRepository.findAll();
        Assert.assertEquals(1, users.size());
        Assert.assertEquals(u.getFirstName(), users.get(0).getFirstName());
        Assert.assertEquals(u.getLastName(), users.get(0).getLastName());
        Assert.assertEquals(u.getUsername(), users.get(0).getUsername());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void changePasswordTest() throws Exception {
        UserDTO u = UserDTO.builder()
                .firstName("Pedro")
                .lastName("Machado")
                .username("p.machado")
                .password(passwordEncoder.encode("1234")).build();
        userService.create(u);

        UserDTO userNovaSenha = UserDTO.builder()
                .username("p.machado")
                .password(passwordEncoder.encode("102030")).build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/user/change")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(userNovaSenha)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(u.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(u.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(u.getUsername()));

        List<User> users = userRepository.findAll();
        Assert.assertEquals(1, users.size());
        Assert.assertEquals(u.getFirstName(), users.get(0).getFirstName());
        Assert.assertEquals(u.getLastName(), users.get(0).getLastName());
        Assert.assertEquals(u.getUsername(), users.get(0).getUsername());
        Assert.assertNotEquals(u.getPassword(), users.get(0).getPassword());
    }

    @Override
    protected Object getController() {
        return userRestController;
    }
}
