/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.EnderecoDTO;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.service.CorreioService;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author lucas.ribeiro
 */
public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected AbstractRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteCreateTest() throws Exception {
        Cliente c = Cliente.builder()
                .nome("nome")
                .rua("Av benno mentz")
                .bairro("Vila ipiranga")
                .cidade("Porto Alegre")
                .estado("RS")
                .telefone("9999")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").telefone("9999").build();
        clienteRepository.saveAndFlush(c);
        Cliente novo = Cliente.builder().id(c.getId()).nome("lucas").telefone("102030").build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/{id}", c.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(novo)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(novo.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(novo.getTelefone()));

        List<Cliente> clientes = clienteRepository.findAll();

        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(novo.getId(), clientes.get(0).getId());
        Assert.assertEquals(novo.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(novo.getTelefone(), clientes.get(0).getTelefone());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindAllTest() throws Exception {
        Cliente c1 = Cliente.builder().nome("nome").telefone("9999").build();
        Cliente c2 = Cliente.builder().nome("lucas").telefone("102030").build();
        clienteRepository.save(c1);
        clienteRepository.save(c2);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].nome").value(c1.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(2));

        List<Cliente> clientes = clienteRepository.findAll();

        Assert.assertEquals(2, clientes.size());
        Assert.assertEquals(c1.getId(), clientes.get(0).getId());
        Assert.assertEquals(c1.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c1.getTelefone(), clientes.get(0).getTelefone());

        Assert.assertEquals(c2.getId(), clientes.get(1).getId());
        Assert.assertEquals(c2.getNome(), clientes.get(1).getNome());
        Assert.assertEquals(c2.getTelefone(), clientes.get(1).getTelefone());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindByIdTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").telefone("9999").build();
        clienteRepository.saveAndFlush(c);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/{id}", c.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()));

        Cliente c2 = clienteRepository.findById(c.getId()).get();

        Assert.assertEquals(c.getId(), c2.getId());
        Assert.assertEquals(c.getNome(), c2.getNome());
        Assert.assertEquals(c.getTelefone(), c2.getTelefone());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteDeleteTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").telefone("9999").build();
        clienteRepository.saveAndFlush(c);
        restMockMvc.perform(MockMvcRequestBuilders.delete("/api/cliente/{id}", c.getId()));

        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(0, clientes.size());

    }
    @MockBean
    private CorreioService correiosService;

    @Test
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void clienteBuscaEnderecoTest() throws Exception {
        EnderecoDTO endereco = EnderecoDTO.builder().rua("Avenida Benno Mentz")
                .bairro("Vila Ipiranga")
                .cidade("Porto Alegre")
                .estado("RS")
                .build();
        Mockito.when(correiosService.buscaEndereco(ArgumentMatchers.anyString())).thenReturn(endereco);
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/cep/91370020"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(endereco.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(endereco.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(endereco.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(endereco.getEstado()));
    }

}