/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author LucasRibeiro
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class UserDTO implements Serializable{
    
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    
}
