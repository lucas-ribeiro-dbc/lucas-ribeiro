/* 
* To change this license header, choose License Headers in Project Properties. 
* To change this template file, choose Tools | Templates 
* and open the template in the editor. 
*/ 
package br.com.dbc.locadora.rest; 
  
import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.entity.User; 
import br.com.dbc.locadora.service.AbstractCrudService; 
import br.com.dbc.locadora.service.UserService; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
  
/** 
* 
* @author LucasRibeiro 
*/ 
@RestController 
@RequestMapping("/api/user") 
public class UserRestController{ 
  
    @Autowired 
    private UserService userService; 
    
    protected UserService getService() { 
        return userService; 
    } 
    
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> post(@RequestBody UserDTO input) {
        return ResponseEntity.ok(getService().create(input));
    }
    
    @GetMapping()
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(getService().findAll(pageable));
    }
    
    @PutMapping("/change")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> put(@RequestBody UserDTO input) {
        return ResponseEntity.ok(getService().changePassword(input));
    }
  
  
} 