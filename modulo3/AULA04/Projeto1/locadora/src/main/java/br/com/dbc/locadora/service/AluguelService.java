/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.AluguelDTOValor;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.repository.AluguelRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lucas.ribeiro
 */
@Service
public class AluguelService extends AbstractCrudService<Aluguel> {

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public AluguelDTOValor retirada(AluguelDTO aluguelDTO) {
        //Transforma Aluguel DTO em um aluguel
        Aluguel aluguel = Aluguel.builder()
                .retirada(LocalDate.now())
                .idCliente((Cliente) clienteService.findById(aluguelDTO.getIdCliente()).get())
                .previsao(LocalDate.now().plusDays(aluguelDTO.getMidias().size()))
                .build();
        //Lista de midias que vao ser retiradas
        List<Midia> midias = new ArrayList<Midia>();
        //Salva aluguel
        getRepository().save(aluguel);
        //Percorre a lista que contem os ids das midias
        for (int i = 0; i < aluguelDTO.getMidias().size(); i++) {
            //Procura a midia correspondente, através do ID
            Midia midia = (Midia) midiaService.findById(aluguelDTO.getMidias().get(i)).get();
            //atribui a midia o ID do aluguel
            midia.setIdAluguel(aluguel);
            //adiciona essa midia nas midias que foram retiradas
            midias.add(midia);
            //salva a midia, agora contendo o ID do aluguel
            midiaService.save(midia);
        }
        //atualiza as midias do aluguel
        aluguel.setMidiaList(midias);
        //atualiza a data de previsao conforme a quantidade de midias que foram retiradas 
        aluguel.setPrevisao(LocalDate.now().plusDays(midias.size()));
        
        List<Long> idMidias = new ArrayList<>();
        for(int i = 0 ; i < aluguel.getMidiaList().size(); i++){
            idMidias.add(aluguel.getMidiaList().get(i).getId());
        }
        double valorTotalMidias = valorMidiaService.findAllValorMidias(idMidias);
        BigDecimal valorAluguel = new BigDecimal(valorTotalMidias).setScale(2, RoundingMode.HALF_EVEN);
        AluguelDTOValor aluguelComValor = AluguelDTOValor.builder()
                    .id(aluguel.getId())
                    .retirada(aluguel.getRetirada())
                    .previsao(aluguel.getPrevisao())
                    .devolucao(aluguel.getDevolucao())
                    .valor(valorAluguel)
                    .idCliente(aluguel.getIdCliente())
                    .build();
        getRepository().save(aluguel);
        return aluguelComValor;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public AluguelDTOValor devolucao(List<Long> idMidias) {
        //Busca a primeira midia que vai ser devolvida
        Midia primeiraMidia = (Midia) midiaService.findById(idMidias.get(0)).get();
        //Pega o id do aluguel das midias que vao ser devolvidas
        Long idAluguel = primeiraMidia.getIdAluguel().getId();
        //Busca o aluguel pelo seu ID
        Aluguel aluguel = (Aluguel) getRepository().findById(idAluguel).get();
        //Midias que vao ser devolvidas, que estão contidas em aluguel
        List<Midia> midias = aluguel.getMidiaList();
        //Midias que devem ser removidas de aluguel após a sua devolucao
        List<Midia> midiasRemove = new ArrayList<>();
        //Percorre a lista de idMidias que vão ser devolvidas
        for (Long idMidia : idMidias) {
            //Percorre as midias contidas em aluguel
            for (Midia midia : midias) {
                //Se a midia contida em aluguel possuir o mesmo id de uma das midias que vão ser devolvidas
                //adiciona essa midia nas midias que devem ser removidas de aluguel 
                if (midia.getId().equals(idMidia)) {
                    midiasRemove.add(midia);
                }
            }
            //Busca a midia pelo seu idMidia e seta o aluguel como null
            //Depois salva a midia com o aluguel null
            Midia midia = (Midia) midiaService.findById(idMidia).get();
            midia.setIdAluguel(null);
            midiaService.save(midia);
        }
        //Pega o valor total das midias, para caso haja multa calcular a multa
        //idMidias = List<Long> ids
        double valorTotalMidias = valorMidiaService.findAllValorMidias(idMidias);
        //Remove todas as midias que foram devolvidas
        midias.removeAll(midiasRemove);
        //Atualiza a lista de midias do aluguel
        aluguel.setMidiaList(midias);
        double multa = 0;
        //se as midias estiverem vazias, significa que todas midias foram devolvidas
        if (midias.isEmpty()) {
            //Atualiza a data de devolucao
            aluguel.setDevolucao(ZonedDateTime.now());
            //Verifica se a data de devolucao é igual a data de previsao
            //Verifica o prazo, que termina as 16h do dia da entrega.
            if (ZonedDateTime.now().toLocalDate().equals(aluguel.getPrevisao()) && ZonedDateTime.now().getHour() > 16) {
                multa = 1;
                //ultrapassou a data de previsão
            } else if (ZonedDateTime.now().toLocalDate().isAfter(aluguel.getPrevisao())) {
                //A quantidade de multas vai ser baseada na quantidade de dias entre a data devolvida e a data prevista
                multa = Period.between(ZonedDateTime.now().toLocalDate(), aluguel.getPrevisao()).getDays();
            }
        }
        //Pega o valor total das midias e multiplica pela quantidade de multa
            BigDecimal valorSemMulta = new BigDecimal(valorTotalMidias).setScale(2, RoundingMode.HALF_EVEN);
            BigDecimal valorAluguel = new BigDecimal(multa*valorTotalMidias).setScale(2, RoundingMode.HALF_EVEN);
            aluguel.setMulta(valorAluguel);
            getRepository().save(aluguel);
            AluguelDTOValor aluguelComValor = AluguelDTOValor.builder()
                    .id(aluguel.getId())
                    .retirada(aluguel.getRetirada())
                    .previsao(aluguel.getPrevisao())
                    .devolucao(aluguel.getDevolucao())
                    .valor(valorSemMulta)
                    .multa(valorAluguel)
                    .total(valorAluguel.add(valorSemMulta).setScale(2, RoundingMode.HALF_EVEN))
                    .idCliente(aluguel.getIdCliente())
                    .build();
            return aluguelComValor;
        }
    
    public List<Aluguel> findByPrevisao(LocalDate previsao){
        List<Aluguel> todosAlugueis = getRepository().findAll();
        ArrayList<Aluguel> alugueisHoje = new ArrayList<>();
        
        for(Aluguel a: todosAlugueis){
            if(a.getPrevisao() == previsao){
                alugueisHoje.add(a);
            }
        }
        return alugueisHoje;        
    }
    
    public Page<Filme> devolucaoHoje(Pageable pageable){
        List<Aluguel> alugueisHoje = findByPrevisao(LocalDate.now());
        ArrayList<Filme> filmes = new ArrayList<>();
        for(Aluguel a: alugueisHoje){
            for(Midia m : a.getMidiaList()){
                filmes.add(m.getIdFilme());
            }
        }
        return new PageImpl<>(filmes, pageable, filmes.size());
    }

    }
