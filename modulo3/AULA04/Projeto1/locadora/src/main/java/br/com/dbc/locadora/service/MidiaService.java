/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lucas.ribeiro
 */
@Service
public class MidiaService extends AbstractCrudService<Midia> {

    @Autowired
    private MidiaRepository midiaRepository;
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<Midia> saveMidia(Filme filme, MidiaDTO midia) {
        //Recebe um filme, e uma MIDIADTO
        //Cria um laço de repetição baseado na quantidade de midas que devem ser inseridas
        ArrayList<Midia> midias = new ArrayList<>();
        for (int i = 0; i < midia.getQuantidade(); i++) {
            //Salva a midia no banco e adiciona ela no array de midias para ser retornada no método
            //toMidia -> Converte a MidiaDTO em uma midia
            midias.add(getRepository().save(midia.toMidia(filme)));
        }
        return midias;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<Midia> updateMidia(Filme filme, MidiaDTO midia) {

        //Pega todas as midias por id do filme e tipo
        //Apenas vhs de tal filme, apenas dvds de tal filme
        List<Midia> midiasEncontradas = findAllByIdFilmeAndType(filme.getId(), midia.getTipo());

        //Se a quantidade de midias ja cadastradas for maior que a nova quantidade    
        List<Midia> deletar = new ArrayList<>();
        long diferenca = midiasEncontradas.size() - midia.getQuantidade();
        if (midiasEncontradas.size() > midia.getQuantidade()) {
            for (int i = 0; i < midiasEncontradas.size(); i++) {
                //se a midia nao estiver locada, faz update
                if (midiasEncontradas.get(i).getIdAluguel() == null && diferenca > deletar.size()) {
                    deletar.add(midiasEncontradas.get(i));
                }else{break;}
            }
            deletar.forEach(d->{
                valorMidiaService.deleteByIdMidia(d);
                delete(d.getId());
                midiasEncontradas.remove(d);
            });
        } //Se a quantidade de midias ja cadastradas for menor que a nova quantidade 
        else if (midiasEncontradas.size() < midia.getQuantidade()) {
            for (int i = 0; i < midia.getQuantidade() - midiasEncontradas.size(); i++) {
                //adiciona novas midias
                midiasEncontradas.add(getRepository().save(midia.toMidia(filme)));
            }
        } 
        System.out.println(midiasEncontradas.size());
        return midiasEncontradas;
    }

    public List<Midia> findAllByIdFilmeAndType(Long id, MidiaType midiaType) {
        ArrayList<Midia> midiasAchadas = new ArrayList<>();
        List<Midia> midias = getRepository().findAll();
        for (Midia m : midias) {
            if (m.getIdFilme().getId() == id && m.getMidiaType() == midiaType) {
                midiasAchadas.add(m);
            }
        }
        return midiasAchadas;
    }
    
    public int countByTipo(MidiaType midiaType){
        List<Midia> midiasCadastradas = getRepository().findAll();
        ArrayList<Midia> midiasDoTipoSelecionado = new ArrayList<>();
        for(Midia m :  midiasCadastradas){
            if(m.getMidiaType() == midiaType){
                midiasDoTipoSelecionado.add(m);
            }
        }
        return midiasDoTipoSelecionado.size();
    }
    public Page<Midia> search(String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoIniFim, Pageable pageable){
        return midiaRepository.findByIdFilmeTituloIgnoreCaseOrIdFilmeCategoriaOrIdFilmeLancamentoBetween(titulo, categoria, lancamentoIni , lancamentoIniFim, pageable);
    }
}
