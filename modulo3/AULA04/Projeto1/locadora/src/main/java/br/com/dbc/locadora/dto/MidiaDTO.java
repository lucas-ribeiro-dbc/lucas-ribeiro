/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author lucas.ribeiro
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class MidiaDTO implements Serializable {
    
    private MidiaType tipo;
    private Long quantidade;
    private BigDecimal valor;
    
    public Midia toMidia(Filme filme){
        return Midia.builder().midiaType(tipo).idAluguel(null).idFilme(filme).build();
    }
    public Midia toMidia(Filme filme, Long id){
        return Midia.builder().id(id).midiaType(tipo).idAluguel(null).idFilme(filme).build();
    }
}
