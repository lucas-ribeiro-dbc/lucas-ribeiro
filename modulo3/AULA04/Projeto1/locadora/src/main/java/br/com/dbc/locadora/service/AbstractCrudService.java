/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lucas.ribeiro
 */
public abstract class AbstractCrudService<E> {
    
   
    protected abstract JpaRepository<E, Long> getRepository();
    
    //save
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save(@NotNull @Valid E e) {
        return getRepository().save(e);
    }

    //delete
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(Long id) {
        getRepository().deleteById(id);
    }
    
    //findById  
    public Optional<E> findById(Long id){
        return getRepository().findById(id);
    }
    
    //findAll
      public Page<E> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }
    
    
}
