/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author lucas.ribeiro
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class EnderecoDTO {
    
    private String bairro;
    private String cidade;
    private String rua;
    private String estado;
}
