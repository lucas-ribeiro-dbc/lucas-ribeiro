/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lucas.ribeiro
 */
@RestController
@RequestMapping("/api/filme")
public class FilmeRestController extends AbstractRestController<FilmeService, Filme> {
    
    @Autowired
    private FilmeService filmeService;

    @Override
    protected FilmeService getService() {
        return filmeService;
    }
    
   @PostMapping("/midia")
   @PreAuthorize("hasAuthority('ADMIN_USER')")
   @Transactional(readOnly = false, rollbackFor = Exception.class)
   public ResponseEntity<?> post(@RequestBody FilmeDTO input) {
       if (input.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
       return ResponseEntity.ok(getService().create(input));
   }
   @PutMapping("/{id}/midia")
   @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody FilmeDTO input) {
        if (id == null || !Objects.equals(input.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(getService().update(input));
    }
    
    @GetMapping("/search")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> search(Pageable pageable,
            @RequestParam(name = "titulo", required = false) String titulo,
            @RequestParam(name = "categoria", required = false) Categoria categoria,
            @RequestParam(name = "lancamentoIni", required = false)@DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(name = "lancamentoFim", required = false)@DateTimeFormat(pattern = "dd/MM/yyyy")  LocalDate lancamentoFim){
          return ResponseEntity.ok(getService().search(titulo, categoria, lancamentoIni, lancamentoFim, pageable));
    }
    
    @GetMapping("count/{id}/{midiaType}")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> countByTipo(@PathVariable Long id, @PathVariable MidiaType midiaType) {
        return ResponseEntity.ok(getService().countByTipo(id, midiaType));
    }
    
        @GetMapping("precos/{id}")
        @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> findPrecoFilme(@PathVariable Long id) {
        return ResponseEntity.ok(getService().findPrecoFilme(id));
    }
    
    @PostMapping("/search/catalogo")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public ResponseEntity<?> get(@RequestBody FilmeDTO filme){
          return ResponseEntity.ok(getService().searchCatalogo(filme.getTitulo()));
    }
}
