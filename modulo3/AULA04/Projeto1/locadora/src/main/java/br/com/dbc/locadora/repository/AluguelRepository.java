/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author lucas.ribeiro
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
    
}
