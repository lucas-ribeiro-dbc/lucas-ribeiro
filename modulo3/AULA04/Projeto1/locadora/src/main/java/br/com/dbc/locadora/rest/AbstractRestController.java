/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.AbstractEntity;
import br.com.dbc.locadora.service.AbstractCrudService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author lucas.ribeiro
 */
@RestController
public abstract class AbstractRestController<SERVICE extends AbstractCrudService<ENTITY>,
        ENTITY extends AbstractEntity<Long>> {
    
    protected abstract SERVICE getService();

    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(getService().findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        return getService().findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody ENTITY input) {
        if (id == null || !Objects.equals(input.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(getService().save(input));
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody ENTITY input) {
        if (input.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(getService().save(input));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        getService().delete(id);
        return ResponseEntity.noContent().build();
    }
}
