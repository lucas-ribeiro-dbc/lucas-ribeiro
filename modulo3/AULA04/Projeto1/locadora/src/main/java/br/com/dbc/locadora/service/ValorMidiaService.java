/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.math.BigDecimal;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lucas.ribeiro
 */
@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia> {

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void savePreco(List<Midia> lista, BigDecimal valor) {
        for (Midia m : lista) {
            getRepository().save(ValorMidia.builder()
                    .idMidia(m)
                    .valor(valor)
                    .inicioVigencia(ZonedDateTime.now())
                    .build());
        }
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public ValorMidia updateFimVigencia(Midia midia) {
        ValorMidia valorMidiaAntigo = findByIdMidia(midia.getId());
        ValorMidia valorMidiaAtualizada = ValorMidia.builder()
                .idMidia(midia)
                .valor(valorMidiaAntigo.getValor())
                .inicioVigencia(valorMidiaAntigo.getInicioVigencia())
                .fimVigencia(ZonedDateTime.now()).build();
        return getRepository().save(valorMidiaAtualizada);
    }

    public ValorMidia findByIdMidia(Long id) {
        List<ValorMidia> valorMidias = getRepository().findAll();
        for (ValorMidia vm : valorMidias) {
            if (vm.getIdMidia().getId() == id) {
                return vm;
            }
        }
        return null;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateValorMidia(List<Midia> lista, BigDecimal valor) {
        for(Midia m : lista){
            ValorMidia valorMidiaAntiga = findByIdMidia(m.getId());
            ValorMidia novoValorMidia = ValorMidia.builder()
                .idMidia(m)
                .valor(valor)
                .inicioVigencia(valorMidiaAntiga.getInicioVigencia())
                .fimVigencia(ZonedDateTime.now()).build();
            getRepository().save(novoValorMidia);
        }
    }

    public List<ValorMidia> findAllByIdFilme(Long id) {
        List<ValorMidia> valorMidias = getRepository().findAll();
        ArrayList<ValorMidia> vms = new ArrayList<>();
        for (ValorMidia vm : valorMidias) {
            if (vm.getIdMidia().getIdFilme().getId() == id) {
                vms.add(vm);
            }
        }
        return vms;
    }
    
    public double findAllValorMidias(List<Long> idMidias){
        double soma = 0;
        List<ValorMidia> valorMidias = getRepository().findAll();
        for(ValorMidia vm : valorMidias ){
              for(Long i: idMidias){
                  //Verifica se a midia do valorMidia possui o mesmo ID de uma das midias que foram passadas
                  //por parametro
                  if(vm.getIdMidia().getId() == i){
                      soma += vm.getValor().doubleValue();
                  }
          }  
        }
        return soma;
       
    }
        public void deleteByIdMidia(Midia midia){
        List<ValorMidia> valorMidias = getRepository().findAll();
        for(ValorMidia vm : valorMidias){
            if(vm.getIdMidia().getId() == midia.getId()){
                getRepository().delete(vm);
            }
        }
        
        
    }
}
