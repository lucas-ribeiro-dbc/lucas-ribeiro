/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lucas.ribeiro
 */
@RestController
@RequestMapping("/api/midia")
public class MidiaRestController extends AbstractRestController<MidiaService, Midia> {
    
    @Autowired
    private MidiaService midiaService;

    @Override
    protected MidiaService getService() {
        return midiaService;
    }
    
    @GetMapping("count/{midiaType}")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> countByTipo(@PathVariable MidiaType midiaType) {
        return ResponseEntity.ok(getService().countByTipo(midiaType));
    }
    
    @GetMapping("/search")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> search(Pageable pageable,
            @RequestParam(name = "titulo", required = false) String titulo,
            @RequestParam(name = "categoria", required = false) Categoria categoria,
            @RequestParam(name = "lancamentoIni", required = false)@DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(name = "lancamentoFim", required = false)@DateTimeFormat(pattern = "dd/MM/yyyy")  LocalDate lancamentoFim){
          return ResponseEntity.ok(getService().search(titulo, categoria, lancamentoIni, lancamentoFim, pageable));
    }
}
