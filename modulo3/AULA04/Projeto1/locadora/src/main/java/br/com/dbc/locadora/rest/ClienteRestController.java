/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.dto.EnderecoDTO;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.CorreioService;
import br.com.dbc.minhaseguradora.ws.ConsultaCEP;
import br.com.dbc.minhaseguradora.ws.ConsultaCEPResponse;
import br.com.dbc.minhaseguradora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lucas.ribeiro
 */
@RestController
@RequestMapping("/api/cliente")
public class ClienteRestController extends AbstractRestController<ClienteService, Cliente> {
    
    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private CorreioService correioService;
    
    @Override
    protected ClienteService getService() {
        return clienteService;
    }

    
    @GetMapping("/cep/{cep}")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public ResponseEntity<?> cep(@PathVariable("cep") String cep) {     
        return ResponseEntity.ok(correioService.buscaEndereco(cep));
    }
    
}
