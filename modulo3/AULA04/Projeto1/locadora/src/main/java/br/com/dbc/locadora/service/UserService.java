/* 
* To change this license header, choose License Headers in Project Properties. 
* To change this template file, choose Tools | Templates 
* and open the template in the editor. 
*/ 
package br.com.dbc.locadora.service; 
  
import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.entity.Role;
import br.com.dbc.locadora.entity.User; 
import br.com.dbc.locadora.repository.UserRepository; 
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.data.jpa.repository.JpaRepository; 
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
  
/** 
* 
* @author LucasRibeiro 
*/ 
@Service
public class UserService extends AbstractCrudService<User>{ 
  
    @Autowired 
    private UserRepository userRepository; 
    
   @Autowired
   private PasswordEncoder passwordEncoder;
    
    @Autowired
    private RoleService roleService;
  
    @Override 
    protected JpaRepository<User, Long> getRepository() { 
        return userRepository; 
    }  
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User create(UserDTO input){
        Role role = roleService.findById(new Long(1)).get();
        ArrayList <Role> roles = new ArrayList<>();
        roles.add(role);
        User usuario = User.builder()
                .firstName(input.getFirstName())
                .lastName(input.getLastName())
                .password(passwordEncoder.encode(input.getPassword()))
                .username(input.getUsername())
                .roles(roles)
                .build();
        return getRepository().save(usuario);
        
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User changePassword(UserDTO input){
        User user = userRepository.findByUsername(input.getUsername());
        user.setPassword(passwordEncoder.encode(input.getPassword()));
        return getRepository().save(user);
    }
    
    
} 