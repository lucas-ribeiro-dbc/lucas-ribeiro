/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author lucas.ribeiro
 */
@Entity
@Table(name = "ALUGUEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aluguel.findAll", query = "SELECT a FROM Aluguel a")
    , @NamedQuery(name = "Aluguel.findById", query = "SELECT a FROM Aluguel a WHERE a.id = :id")
    , @NamedQuery(name = "Aluguel.findByRetirada", query = "SELECT a FROM Aluguel a WHERE a.retirada = :retirada")
    , @NamedQuery(name = "Aluguel.findByPrevisao", query = "SELECT a FROM Aluguel a WHERE a.previsao = :previsao")
    , @NamedQuery(name = "Aluguel.findByDevolucao", query = "SELECT a FROM Aluguel a WHERE a.devolucao = :devolucao")
    , @NamedQuery(name = "Aluguel.findByMulta", query = "SELECT a FROM Aluguel a WHERE a.multa = :multa")})
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Aluguel extends AbstractEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @SequenceGenerator(name = "ALUGUEL_SEQ", sequenceName = "ALUGUEL_SEQ", allocationSize = 1 )
    @GeneratedValue(generator = "ALUGUEL_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RETIRADA")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate retirada;
    @Basic(optional = false)
    @Column(name = "PREVISAO")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate previsao;
    @Column(name = "DEVOLUCAO")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private ZonedDateTime devolucao;
    @Column(name = "MULTA")
    private BigDecimal multa;
    @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Cliente idCliente;
    @JsonIgnore
    @OneToMany(mappedBy = "idAluguel")
    private List<Midia> midiaList;
    
}
