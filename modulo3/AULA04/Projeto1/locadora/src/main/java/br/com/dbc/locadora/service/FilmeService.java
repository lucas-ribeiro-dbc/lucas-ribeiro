/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lucas.ribeiro
 */
@Service
public class FilmeService extends AbstractCrudService<Filme> {

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Autowired
    private AluguelService aluguelService;

    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }

    //Salvar filme completo, recebe DTO  de filme
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Filme> create(@NotNull @Valid FilmeDTO input) {

        //Converte DTO para filme e realiza o save
        Filme filme = getRepository().save(Filme.builder()
                .id(input.getId())
                .titulo(input.getTitulo())
                .categoria(input.getCategoria())
                .lancamento(input.getLancamento())
                .build());

        ArrayList<Midia> midiasDoFilme = new ArrayList<>();

        //Percorre as midiasDTO do filmeDTO
        for (int i = 0; i < input.getMidia().size(); i++) {
            //salva as midias no midiaService e retorna a lista de midias inseridas
            List<Midia> midiasRecemInseridas = midiaService.saveMidia(filme, input.getMidia().get(i));
            //Percorre as midias inseridas e adiciona na lista de midias do filme
            for (Midia m : midiasRecemInseridas) {
                midiasDoFilme.add(m);
            }
            //salva valorMidia no valorMidiaService 
            valorMidiaService.savePreco(midiasRecemInseridas, input.getMidia().get(i).getValor());
        }

        //Atualiza as midias do filme, pois quando foi salvo no inicio no método não foi inserido
        filme.setMidiaList(midiasDoFilme);
        //chamar update pra atualizar lista de midias
        return super.findById(filme.getId());
    }
    //filme = save(filme)
    //salva midia no midia service
    //for dto.midias{
    //midias = midiaService.saveMidia(filme, dto.getQuantidade())
    //salva preco da midia no ValorMidiaService
    //valorMidiaService.savePreco(midias, dto.getPreco())
    //}
    //return super.find(filme.id)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Filme> update(@NotNull @Valid FilmeDTO input) {

        //Converte DTO para filme e realiza o save
        Filme filme = getRepository().save(Filme.builder()
                .id(input.getId())
                .titulo(input.getTitulo())
                .categoria(input.getCategoria())
                .lancamento(input.getLancamento())
                .build());

        //Percorre as midiasDTO do filmeDTO
        for (int i = 0; i < input.getMidia().size(); i++) {
            //atualiza as midias no midiaService e retorna a lista de midias atualizadas
            List<Midia> midiasAtualizadas = midiaService.updateMidia(filme, input.getMidia().get(i));
            //Percorre as midias atualizadas e adiciona na lista de midias do filme
            //atualiza valorMidia no valorMidiaService 
            valorMidiaService.updateValorMidia(midiasAtualizadas, input.getMidia().get(i).getValor());
        }

        //Atualiza as midias do filme, pois quando foi salvo no inicio no método não foi inserido
        //chamar update pra atualizar lista de midias
        return super.findById(filme.getId());
    }

    public Page<Filme> search(String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoIniFim, Pageable pageable) {
        return filmeRepository.findByTituloIgnoreCaseOrCategoriaOrLancamentoBetween(titulo, categoria, lancamentoIni, lancamentoIniFim, pageable);
    }

    public int countByTipo(Long id, MidiaType midiaType) {
        List<Midia> midias = midiaService.findAllByIdFilmeAndType(id, midiaType);
        return midias.size();
    }

    public List<ValorMidia> findPrecoFilme(Long id) {
        List<Midia> midias = midiaService.getRepository().findAll();
        List<ValorMidia> vms = valorMidiaService.getRepository().findAll();

        List<ValorMidia> vmsEncontradas = new ArrayList<>();

        for (Midia m : midias) {
            if (m.getIdFilme().getId() == id) {
                for (ValorMidia vm : vms) {
                    if (vm.getIdMidia() == m) {
                        vmsEncontradas.add(vm);
                    }
                }
            }
        }
        return vmsEncontradas;
    }

    public List<CatalogoDTO> searchCatalogo(String titulo) {
        ArrayList<CatalogoDTO> catalogo = new ArrayList<>();
        LocalDate previsao = null;
        List<ValorMidia> vms = valorMidiaService.getRepository().findAll();
        List<Aluguel> alugueis = aluguelService.getRepository().findAll();
        List<Midia> midias = midiaRepository.findByIdFilmeTituloContaining(titulo);
        for (Midia m : midias) {
            //se a midia nao estiver alugada
            if (m.getIdAluguel() == null) {
                for (ValorMidia vm : vms) {
                    if (m.getId() == vm.getIdMidia().getId()) {
                        catalogo.add(CatalogoDTO.builder()
                                .titulo(m.getIdFilme().getTitulo())
                                .disponivel(true)
                                .previsao(null)
                                .precoAluguel(vm.getValor())
                                .midiaType(m.getMidiaType())
                                .build());
                    }
                }
            } //se a midia estiver alugada
            else {
                for (Aluguel a : alugueis) {
                    if (a.getId() == m.getIdAluguel().getId()) {
                        previsao = a.getPrevisao();
                        break;
                    }
                }
                for (ValorMidia vm : vms) {
                    if (m.getId() == vm.getIdMidia().getId()) {
                        catalogo.add(CatalogoDTO.builder()
                                .titulo(m.getIdFilme().getTitulo())
                                .disponivel(false)
                                .precoAluguel(vm.getValor())
                                .previsao(previsao)
                                .midiaType(m.getMidiaType())
                                .build());
                    }
                }
            }
        }
        return catalogo;
    }
}
