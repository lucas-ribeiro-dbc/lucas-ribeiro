/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author lucas.ribeiro
 */
@Entity
@Table(name = "VALOR_MIDIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValorMidia.findAll", query = "SELECT v FROM ValorMidia v")
    , @NamedQuery(name = "ValorMidia.findById", query = "SELECT v FROM ValorMidia v WHERE v.id = :id")
    , @NamedQuery(name = "ValorMidia.findByValor", query = "SELECT v FROM ValorMidia v WHERE v.valor = :valor")
    , @NamedQuery(name = "ValorMidia.findByInicioVigencia", query = "SELECT v FROM ValorMidia v WHERE v.inicioVigencia = :inicioVigencia")
    , @NamedQuery(name = "ValorMidia.findByFimVigencia", query = "SELECT v FROM ValorMidia v WHERE v.fimVigencia = :fimVigencia")})
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ValorMidia extends AbstractEntity<Long>  implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @SequenceGenerator(name = "VALOR_MIDIA_SEQ", sequenceName = "VALOR_MIDIA_SEQ", allocationSize = 1 )
    @GeneratedValue(generator = "VALOR_MIDIA_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INICIO_VIGENCIA")
    private ZonedDateTime inicioVigencia;
    @Column(name = "FIM_VIGENCIA")
    private ZonedDateTime fimVigencia;
    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Midia idMidia;

    
}
