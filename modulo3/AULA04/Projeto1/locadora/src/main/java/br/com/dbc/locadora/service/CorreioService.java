/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.dto.EnderecoDTO;
import br.com.dbc.minhaseguradora.ws.ConsultaCEP;
import br.com.dbc.minhaseguradora.ws.ConsultaCEPResponse;
import br.com.dbc.minhaseguradora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas.ribeiro
 */
@Service
public class CorreioService {
    
     private final String URL = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";

    @Autowired 
    SoapConnector soapConnector;
    
    @Autowired 
    ObjectFactory objectFactory;
    
     public EnderecoDTO buscaEndereco(String cep){
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);
        ConsultaCEPResponse resultado = ((JAXBElement<ConsultaCEPResponse>)  soapConnector
                .callWebService(URL, objectFactory.createConsultaCEP(consulta))).getValue();
        EnderecoDTO endereco = EnderecoDTO.builder()
                .bairro(resultado.getReturn().getBairro())
                .cidade(resultado.getReturn().getCidade())
                .estado(resultado.getReturn().getUf())
                .rua(resultado.getReturn().getEnd())
                .build();    
        return endereco;
    }
    
}
