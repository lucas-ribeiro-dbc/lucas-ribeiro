/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Cliente;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author lucas.ribeiro
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AluguelDTOValor implements Serializable{
    
    private Long id;
    private LocalDate retirada;
    private LocalDate previsao;
    private ZonedDateTime devolucao;         
    private Cliente idCliente;
    private BigDecimal valor;
    private BigDecimal multa;
    private BigDecimal total;
    
}
