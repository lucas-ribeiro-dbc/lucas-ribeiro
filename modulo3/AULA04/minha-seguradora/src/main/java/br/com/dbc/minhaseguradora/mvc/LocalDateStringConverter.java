package br.com.dbc.minhaseguradora.mvc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author tiago
 */
public class LocalDateStringConverter implements Converter<LocalDate, String> {

    @Override
    public String convert(LocalDate locDate) {
        return (locDate == null ? null : locDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }
}
