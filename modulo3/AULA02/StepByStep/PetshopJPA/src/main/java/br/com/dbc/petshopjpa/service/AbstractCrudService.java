/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.AbstractDAO;
import java.util.List;

/**
 *
 * @author tiago
 */
public abstract class AbstractCrudService<E, I, DAO extends AbstractDAO<E,I>> {
    
    public abstract DAO getDAO();

    public List<E> findAll() {
        return getDAO().findAll();
    }
    
    public E findOne(I id) {
        return getDAO().findOne(id);
    }
    
    public void create(E cliente){
        getDAO().create(cliente);
    }
    
    public void update(E cliente){
        getDAO().update(cliente);
    }
    
    public void delete(I id){
        getDAO().delete(id);
    }
    
}
