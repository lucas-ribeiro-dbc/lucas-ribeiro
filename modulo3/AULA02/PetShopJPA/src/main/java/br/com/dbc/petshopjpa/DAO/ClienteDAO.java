/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.DAO;

import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author lucas.ribeiro
 */
public class ClienteDAO extends AbstractDAO<Cliente, Long> {

    @Override
    protected Class<Cliente> getEntityClass() {
        return Cliente.class;
    }
    
}
