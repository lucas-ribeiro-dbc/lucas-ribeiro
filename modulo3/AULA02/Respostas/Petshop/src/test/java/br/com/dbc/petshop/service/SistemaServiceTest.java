/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.exercicio.Sistema;
import br.com.dbc.petshop.entity.exercicio.UsuarioSistema;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class SistemaServiceTest {
    
    public SistemaServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.close();
        if(!PersistenceUtils.getEm().getTransaction().isActive())
            PersistenceUtils.getEm().getTransaction().begin();
        PersistenceUtils.getEm().createNativeQuery("DELETE FROM USUARIO_SISTEMA").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("DELETE FROM USUARIO").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("DELETE FROM SISTEMA").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }
    
    private void createSistema(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.merge(new Sistema("nome", null));
        t.commit();
        session.close();
    }

    /**
     * Test of findAll method, of class SistemaService.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        createSistema();
        SistemaService instance = SistemaService.getInstance();
        List<Sistema> result = instance.findAll();
        assertEquals(1, result.size());
        assertTrue(result.get(0).getNome().equalsIgnoreCase("nome"));
    }

    /**
     * Test of findAllCriteria method, of class SistemaService.
     */
    @Test
    public void testFindAllCriteria() {
        System.out.println("findAllCriteria");
        createSistema();
        SistemaService instance = SistemaService.getInstance();
        List<Sistema> result = instance.findAllCriteria();
        assertEquals(1, result.size());
        assertTrue(result.get(0).getNome().equalsIgnoreCase("nome"));
    }

    /**
     * Test of create method, of class SistemaService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        Sistema c = new Sistema("nome", null);
        SistemaService instance = SistemaService.getInstance();
        Sistema result = instance.create(c);
        assertTrue(result.getId() != null);
    }

    /**
     * Test of createCriteria method, of class SistemaService.
     */
    @Test
    public void testCreateCriteria() {
        System.out.println("createCriteria");
        Sistema c = new Sistema("nome", null);
        SistemaService instance = SistemaService.getInstance();
        Sistema result = instance.createCriteria(c);
        assertTrue(result.getId() != null);
    }
    
    @Test
    public void testInsere2SistemasCom5Usuarios(){
        SistemaService instance = SistemaService.getInstance();
        instance.insere2SistemasCom5Usuarios();
        List<Sistema> sistemas = PersistenceUtils.getEm()
                .createQuery("select s from Sistema s", Sistema.class)
                .getResultList();
        assertEquals(2, sistemas.size());
        sistemas.forEach(s->{
            assertEquals(5, s.getUsuarioSistemaList().size());
        });
    }
    
    @Test
    public void testHasAccess(){
        SistemaService instance = SistemaService.getInstance();
        List<Sistema> sistemas = instance.insere2SistemasCom5Usuarios();
        List<UsuarioSistema> usuarioSistemaFilho = sistemas
                .stream()
                .filter(s->s.getSistemaPai()!=null)
                .flatMap(s->s.getUsuarioSistemaList().stream())
                .collect(Collectors.toList());
        List<UsuarioSistema> usuarioSistemaPai = sistemas
                .stream()
                .filter(s->s.getSistemaPai()==null)
                .flatMap(s->s.getUsuarioSistemaList().stream())
                .collect(Collectors.toList());
        
        Boolean acesso = instance.hasAccess(
                usuarioSistemaFilho.get(0).getIdSistema().getId(), 
                usuarioSistemaFilho.get(0).getIdUsuario().getId());
        assertEquals("Acesso filho filho", true, acesso);
        acesso = instance.hasAccess(
                usuarioSistemaFilho.get(0).getIdSistema().getSistemaPai().getId(), 
                usuarioSistemaFilho.get(0).getIdUsuario().getId());
        assertEquals("Acesso pai filho", false, acesso);
        acesso = instance.hasAccess(
                usuarioSistemaFilho.get(0).getIdSistema().getId(), 
                usuarioSistemaFilho.get(0).getIdSistema().getSistemaPai().getUsuarioSistemaList().get(0).getIdUsuario().getId());
        assertEquals("Acesso filho pai", true, acesso);
        acesso = instance.hasAccess(
                usuarioSistemaPai.get(0).getIdSistema().getId(), 
                usuarioSistemaPai.get(0).getIdUsuario().getId());
        assertEquals("Acesso pai pai", true, acesso);
    }
}
