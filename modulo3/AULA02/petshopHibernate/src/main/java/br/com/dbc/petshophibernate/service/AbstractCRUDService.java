/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.AbstractDAO;
import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.entity.AbstractEntity;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.List;
import javassist.NotFoundException;

/**
 *
 * @author lucas.ribeiro
 */
public abstract class AbstractCRUDService <ENTITY extends AbstractEntity<ID>, 
        ID, DAO extends AbstractDAO<ENTITY, ID>> {
    protected abstract DAO getDAO();
    public List<ENTITY> findAll(){
        return getDAO().findAll();
    }
    public ENTITY findOne(ID id){
        System.out.println("Envia email que buscou o id: " + id);
        return getDAO().findById(id);
    }
    public void create(ENTITY entity){
        if(entity.getId()!= null){
            throw new IllegalArgumentException("Criação não pode ter ID");
        }
        getDAO().createOrUpdate(entity);
    }
      public void update(ENTITY entity){
        if(entity.getId() == null){
            throw new IllegalArgumentException("Atualização deve ter ID");
        }
       getDAO().createOrUpdate(entity);
    }
      public void delete(ID id) throws NotFoundException{
        if(id == null){
            throw new IllegalArgumentException("ID deve ser informado");
        }
        ENTITY entity = getDAO().findById(id);
        if(entity == null){
            throw new NotFoundException("Id informado não foi encontrado");
        }
        getDAO().delete(entity);
    }
    
    
}
