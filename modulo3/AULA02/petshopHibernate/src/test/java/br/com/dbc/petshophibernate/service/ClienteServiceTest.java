
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.dao.HibernateUtil;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreate() {
        System.out.println("create");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        Assert.assertEquals("Quantidade de clientes errada", 1, clientes.size());
        Cliente result = clientes.stream().findAny().get();
        assertEquals("Cliente diferente", cliente1.getId(), result.getId());
        assertEquals("Quantidade animais diferente",
                cliente1.getAnimalList().size(),
                result.getAnimalList().size());
        assertEquals("Animais diferente",
                cliente1.getAnimalList().stream().findAny().get().getId(),
                result.getAnimalList().stream().findAny().get().getId());
        session.close();
    }

    @Test
    public void testCreateMocked() {
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente1);
        Mockito.verify(daoMock, times(1)).createOrUpdate(cliente1);
    }

    @Test(expected = HibernateException.class)
    public void testCreateMockedHibernateException() {
        System.out.println("create mocked HibernateException");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(new HibernateException("")).when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalArgumentException() {
        System.out.println("create IllegalArgumentException");
        ClienteService.getInstance()
                .create(
                        Cliente.builder().id(1l).build()
                );
    }

    @Test
    public void testUpdateMocked() {
        System.out.println("update mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.update(cliente1);
        Mockito.verify(daoMock, times(1)).createOrUpdate(cliente1);
    }

    @Test(expected = HibernateException.class)
    public void testUpdateMockedHibernateException() {
        System.out.println("update mocked HibernateException");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(new HibernateException("")).when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.update(cliente1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalArgumentException() {
        System.out.println("update IllegalArgumentException");
        ClienteService.getInstance()
                .update(
                        Cliente.builder().build()
                );
    }

    @Test
    public void testDeleteMocked() throws NotFoundException {
        System.out.println("delete mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(cliente1);
        Mockito.doReturn(cliente1).when(daoMock).findById(cliente1.getId());

        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);

        clienteService.delete(cliente1.getId());

        Mockito.verify(daoMock, times(1)).findById(cliente1.getId());
        Mockito.verify(daoMock, times(1)).delete(cliente1);
    }

    @Test(expected = HibernateException.class)
    public void testDeleteMockedHibernateException() throws NotFoundException {
        System.out.println("delete mocked HibernateException");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(new HibernateException("")).when(daoMock).delete(cliente1);
        Mockito.doReturn(cliente1).when(daoMock).findById(cliente1.getId());

        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);

        clienteService.delete(cliente1.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIllegalArgumentException() throws NotFoundException, NotFoundException, NotFoundException, NotFoundException {
        System.out.println("delete IllegalArgumentException");
        ClienteService.getInstance()
                .delete(null);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteNotFoundException() throws NotFoundException {
        System.out.println("delete NotFoundException");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(cliente1);
        Mockito.doReturn(null).when(daoMock).findById(cliente1.getId());
        
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        
        clienteService.delete(cliente1.getId());

    }
    
    @Test
    public void testFindOneMocked() {
        System.out.println("findOne mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doReturn(cliente1).when(daoMock).findById(cliente1.getId());

        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);

        clienteService.findOne(cliente1.getId());
        
        Mockito.verify(daoMock, times(1)).findById(cliente1.getId());
    }
    
    @Test(expected = HibernateException.class)
    public void testFindOneMockedHibernateException() {
        System.out.println("findOne mocked HibernateException ");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(new HibernateException("")).when(daoMock).findById(cliente1.getId());

        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);

        clienteService.findOne(cliente1.getId());
        
    }
    
    @Test
    public void testFindAll() {
        System.out.println("findAll mocked");
        List<Cliente> clienteList = Arrays.asList(Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build());
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doReturn(clienteList).when(daoMock).findAll();

        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);

        List<Cliente> returned = clienteService.findAll();
        
        Mockito.verify(daoMock, times(1)).findAll();
        assertEquals(clienteList.size(), returned.size());
    }
    
    @Test(expected = HibernateException.class)
    public void testFindAllMockedHibernateException() {
        System.out.println("findAll mocked HibernateException");
        List<Cliente> clienteList = Arrays.asList(Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build());
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(new HibernateException("")).when(daoMock).findAll();

        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);

        List<Cliente> returned = clienteService.findAll();
        
    }

}