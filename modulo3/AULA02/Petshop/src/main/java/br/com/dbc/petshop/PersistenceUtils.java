/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author lucas.ribeiro
 */
public class PersistenceUtils {
   private static EntityManager em;
   public static EntityManager getEm(){
       if(em == null){
           EntityManagerFactory emf = Persistence.createEntityManagerFactory("petshop");
           em = emf.createEntityManager();
       }
       return em;
   }    
}
