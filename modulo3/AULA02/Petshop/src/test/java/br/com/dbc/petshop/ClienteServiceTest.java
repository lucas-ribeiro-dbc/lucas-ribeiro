/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.SexoType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.ribeiro
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    @Test
    public void testSomaDosAnimais(){
        ClienteService instance = ClienteService.getInstance();
        AnimalService instanceAnimal = AnimalService.getInstance();
        List<Cliente> clientes = instance.buscarTodosClientes();
        //Nao e necessarios adicionar clientes pois no AnimalServiceTest ja 
        //estao sendo incluidos 10 clientes
        //Um unico cliente
        //Vai ser 4500 pois nos outros testes ja foram adicionados clientes
        assertEquals(instance.somaDosAnimais(clientes.get(0)), 45000, 0);
        assertEquals("Cliente: 0", clientes.get(0).getNome());
    }
    @Test
    public void testBuscarClientePorNome() {
        System.out.println("buscarClientePorNome");
        String nome = "%di%";
        String resultado = "Odin"; 
        ClienteService instance = ClienteService.getInstance();
        instance.adicionarUmCliente(new Cliente("Odin", SexoType.M, "Engenheiro"));
        List<Cliente> result = instance.buscarClientePorNome(nome);
        assertEquals(resultado, result.get(0).getNome());
    }
    @Test
    public void testAdicionarClientes(){
        ArrayList<Cliente> lista = new ArrayList<>();
        ClienteService instance = ClienteService.getInstance();
        int tamanhoInicial = instance.buscarTodosClientes().size();
        for(int i = 10; i < 20; i++){
            lista.add(new Cliente("Cliente: "+i, SexoType.M, "Profissao: "+i));
        }
        instance.adicionarClientes(lista);
        List<Cliente> result = instance.buscarTodosClientes();
        assertEquals(tamanhoInicial+10, result.size());     
    }
    
    @Test
    public void testSomarValoresClientes(){
        //Usando jpa e sum
        BigDecimal resultado = ClienteService.getInstance().somaDoValorDosAnimais(new Long(1));  
        assertTrue(4500 == resultado.doubleValue());
    
    }
//    @Test
//    public void testSomaDoValorDosAnimaisHibernate(){
//        //usando hibernate e projections
//        BigDecimal resultado = ClienteService.getInstance().somaDoValorDosAnimaisHibernate(new Long(1));  
//        assertTrue(4500 == resultado.doubleValue());
//    }
    
}
