/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.SexoType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LucasRibeiro
 */
public class AnimalServiceTest {
    
    public AnimalServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of adicionar10AnimaisCliente method, of class AnimalService.
     */
    @Test
    public void testAdicionar10AnimaisCliente() {
        System.out.println("adicionar10AnimaisCliente");
        AnimalService instance = AnimalService.getInstance();
        ArrayList<Animal> listaAnimal = new ArrayList<>();
        instance.adicionar10AnimaisCliente();
        List<Animal> result = instance.buscarTodosAnimais();
        List<Cliente> resultCliente = ClienteService.getInstance().buscarTodosClientes();
        String animal = result.get(0).getNome();
        String animalCliente = resultCliente.get(0).getAnimalList().get(0).getNome();
        assertEquals(animal, animalCliente);
    }
    
}
