/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author lucas.ribeiro
 */
public class ClienteService {
   private static ClienteService instance;
   public static ClienteService getInstance(){
       return instance == null ? new ClienteService() : instance;
   }
   private ClienteService(){}
   
   public List<Cliente> buscarClientePorNome(String nome){
       return PersistenceUtils.getEm()
               .createQuery("SELECT c FROM Cliente c WHERE c.nome like :nome", Cliente.class)
               .setParameter("nome", nome)
               .getResultList();
   }
   //JPA E SUM
   public BigDecimal somaDoValorDosAnimais(Long id){
      return (BigDecimal) PersistenceUtils.getEm()
                .createQuery("SELECT SUM(a.valor) AS valor FROM Animal a where a.idCliente = :idCliente")
                .setParameter("idCliente", new Cliente(id))
                .getSingleResult();
   }
   //HIBERNATE E PROJECTIONS
   public BigDecimal somaDoValorDosAnimaisHibernate(Long id){
      return (BigDecimal) HibernateUtil.getSessionFactory()
                .openSession()
                .createCriteria(Animal.class)
                .setProjection(Projections.projectionList().add(Projections.sum("valor")))
                .add(Restrictions.eq("idCliente", id))
                .uniqueResult();
    }

   public void adicionarUmCliente(Cliente c){
       PersistenceUtils.getEm().getTransaction().begin();
       PersistenceUtils.getEm().persist(c);
       PersistenceUtils.getEm().getTransaction().commit();
   }
   public void adicionarClientes(List<Cliente> lista){
       PersistenceUtils.getEm().getTransaction().begin();
       for(int i = 0; i < lista.size(); i++){
        PersistenceUtils.getEm().persist(lista.get(i));
       }
       PersistenceUtils.getEm().getTransaction().commit();
       
   } 
   public List<Cliente> buscarTodosClientes(){
       return PersistenceUtils.getEm()
               .createQuery("SELECT c from Cliente c", Cliente.class)
               .getResultList();
   }
   
   public String somaDosAnimais(List<Cliente> clientes){
        double soma = 0;
        String total = "";
        for(int i = 0; i < clientes.size();i++){
            soma = somaDosAnimais(clientes.get(i));
            total += "Cliente: "+i +  " Soma dos animais: " + soma+ "\n";
            soma = 0;
        }
        return total;
    }
    public double somaDosAnimais(Cliente c){
        double soma = 0;
            for(int j = 0; j < c.getAnimalList().size();j++){
            soma += Double.parseDouble(String.valueOf(c.getAnimalList().get(j).getValor()));
        }
        return soma;
    }
}
