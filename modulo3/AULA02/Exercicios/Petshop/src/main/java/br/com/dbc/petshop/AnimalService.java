/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.SexoType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LucasRibeiro
 */
public class AnimalService {
   private static AnimalService instance;
   public static AnimalService getInstance(){
       return instance == null ? new AnimalService() : instance;
   }
   private AnimalService(){}
    
    public void adicionar10AnimaisCliente() {
        ArrayList<Animal> listaAnimal = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Cliente c = new Cliente("Cliente: " + i, SexoType.M, "Profissao: " + i);
            ClienteService.getInstance().adicionarUmCliente(c);
        }
        //Clientes so vao possuir id qnd forem inseridos no banco
        List<Cliente> listaClientes = ClienteService.getInstance().buscarTodosClientes();
        listaClientes.get(0).getNome();
        for(int x = 0; x< listaClientes.size(); x++){
            for (int j = 0; j < 10; j++) {
                Animal a = new Animal("Animal: " + j, SexoType.M, new BigDecimal(100*j));
                a.setIdCliente(listaClientes.get(x));
                adicionarUmAnimal(a);
                listaAnimal.add(a);
        }
            listaClientes.get(x).setAnimalList(listaAnimal);
        }

    }
    public void adicionarUmAnimal(Animal a){
        PersistenceUtils.getEm().getTransaction().begin();
                PersistenceUtils.getEm().persist(a);
                PersistenceUtils.getEm().getTransaction().commit();   
    }
    public List<Animal> buscarTodosAnimais() {
        return PersistenceUtils.getEm()
                .createQuery("SELECT a from Animal a", Animal.class)
                .getResultList();
    }
    
}
