/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exercicio02;

import com.mycompany.exercicio02.entity.Produto;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.ribeiro
 */
public class ProdutoServiceTest {
    
    public ProdutoServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testbuscarProdutoPorDescricao() {
        System.out.println("buscarPaisPorDescricao");
        String descricao = "%cla%";
        String resultado = "Teclado";
        ProdutoService instance = ProdutoService.getInstance();
        List<Produto> result = instance.buscarProdutoPorDescricao(descricao);
        assertEquals(resultado, result.get(0).getDescricao());

    }
    
}
