/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exercicio02.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author lucas.ribeiro
 */
@Entity
public class Nota {
    
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(generator = "NOTA_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "NOTA_SEQ", sequenceName = "NOTA_SEQ", allocationSize = 1)
    private Long id;
    
    @Column(name ="DESCRICAO")
    @Basic(optional = false)
    private String descricao;
    
    @OneToMany(mappedBy = "idNota")
    private List<ItemNota> itensNota;
    
    public Nota(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "Nota{" + "id=" + id + ", descricao=" + descricao + ", itensNota=" + itensNota + '}';
    }
    
    
}
