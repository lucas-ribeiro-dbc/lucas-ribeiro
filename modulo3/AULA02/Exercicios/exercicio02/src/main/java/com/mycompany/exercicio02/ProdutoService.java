/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exercicio02;

import com.mycompany.exercicio02.entity.Produto;
import java.util.List;

/**
 *
 * @author lucas.ribeiro
 */
public class ProdutoService {
    
   private static ProdutoService instance;
   public static ProdutoService getInstance(){
       return instance == null ? new ProdutoService() : instance;
   }
   private ProdutoService(){}
   
   public List<Produto> buscarProdutoPorDescricao(String descricao){
       return PersistenceUtil.getEm()
               .createQuery("SELECT p FROM Produto p WHERE p.descricao like :descricao", Produto.class)
               .setParameter("descricao", descricao)
               .getResultList();
   }
}
