/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exercicio02.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


/**
 *
 * @author lucas.ribeiro
 */
@Entity
class ItemNota implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(generator = "ITEMNOTA_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "ITEMNOTA_SEQ", sequenceName = "ITEMNOTA_SEQ", allocationSize = 1)
    private Long ID;
    
    @Column(name = "DESCRICAO")
    @Basic(optional = false)
    private String descricao;
    
    @JoinColumn(name = "ID_NOTA", referencedColumnName = "ID")
    @ManyToOne
    private Nota idNota;
    
    @JoinColumn(name = "ID_PRODUTO", referencedColumnName = "ID")
    @ManyToOne
    private Produto idProduto;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public Nota getIdNota() {
        return idNota;
    }

    public void setIdNota(Nota idNota) {
        this.idNota = idNota;
    }

    @Override
    public String toString() {
        return "ItemNota{" + "ID=" + ID + ", descricao=" + descricao + ", idProduto=" + idProduto + '}';
    }
    
}
