/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dia02.slc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author lucas.ribeiro
 */
public class PersistenceUtil {
     
   private static EntityManager em;
   public static EntityManager getEm(){
       if(em == null){
           EntityManagerFactory emf = Persistence
                   .createEntityManagerFactory("dia2_pu");
           em = emf.createEntityManager();
       }
       return em;
   }
    
}
