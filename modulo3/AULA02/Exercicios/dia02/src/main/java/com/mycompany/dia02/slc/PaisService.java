/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dia02.slc;

import antlr.StringUtils;
import com.mycompany.dia02.entity.Pais;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author lucas.ribeiro
 */
public class PaisService {
   private static PaisService instance;

   public static PaisService getInstance(){
       return instance == null ? new PaisService() : instance;
   }
   private PaisService(){}

   public List<Pais> buscarPaisPorNome(String nome){
       return PersistenceUtil.getEm()
               .createQuery("SELECT p FROM Pais p WHERE p.nome like :nome", Pais.class)
               .setParameter("nome", nome)
               .getResultList();
   }

}
