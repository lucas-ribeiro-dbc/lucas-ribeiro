/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dia02.slc;

import com.mycompany.dia02.entity.Pais;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.ribeiro
 */
public class PaisServiceTest {
    
    public PaisServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of buscarPaisPorNome method, of class PaisService.
     */
    @Test
    public void testBuscarPaisPorNome() {
        System.out.println("buscarPaisPorNome");
        String nome = "%rasi%";
        PaisService instance = PaisService.getInstance();
        String expNome = "Brasil";
        List<Pais> result = instance.buscarPaisPorNome(nome);
        assertEquals(1, result.size());
        assertEquals(expNome, result.get(0).getNome());
    }
    
}
