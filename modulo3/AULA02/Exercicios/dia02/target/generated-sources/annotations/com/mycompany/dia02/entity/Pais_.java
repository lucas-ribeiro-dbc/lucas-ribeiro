package com.mycompany.dia02.entity;

import com.mycompany.dia02.entity.Estado;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-10T13:55:47")
@StaticMetamodel(Pais.class)
public class Pais_ { 

    public static volatile SingularAttribute<Pais, String> sigla;
    public static volatile ListAttribute<Pais, Estado> estadoList;
    public static volatile SingularAttribute<Pais, String> nome;
    public static volatile SingularAttribute<Pais, Long> id;

}