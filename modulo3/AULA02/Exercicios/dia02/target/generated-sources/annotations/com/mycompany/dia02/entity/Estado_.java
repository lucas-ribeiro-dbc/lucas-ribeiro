package com.mycompany.dia02.entity;

import com.mycompany.dia02.entity.Cidade;
import com.mycompany.dia02.entity.Pais;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-10T13:55:47")
@StaticMetamodel(Estado.class)
public class Estado_ { 

    public static volatile ListAttribute<Estado, Cidade> cidadeList;
    public static volatile SingularAttribute<Estado, String> sigla;
    public static volatile SingularAttribute<Estado, Pais> idPais;
    public static volatile SingularAttribute<Estado, String> nome;
    public static volatile SingularAttribute<Estado, Long> id;

}