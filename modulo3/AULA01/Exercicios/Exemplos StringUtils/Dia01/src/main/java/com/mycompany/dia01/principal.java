/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dia01;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author LucasRibeiro
 */
public class principal {
    public static void main(String[]args){
        String a = "Lucas";
        String b = "Lucas";
        String c = "";
        String d = "Ribeiro";
        String e = "eir";
        String f = " hahahaha ";
        Boolean branco = StringUtils.isBlank(e);
        System.out.println("Resultado isBlank: " + branco);
        String leftPad = StringUtils.leftPad(a, 10);
        System.out.println("Resultado LeftPad:----" + leftPad + "---");
        String rightPad = StringUtils.rightPad(b, 10);
        System.out.println("Resultado RightPad:----" + rightPad+ "---");
        String trim = StringUtils.trimToNull(f);
        System.out.println("Resultado TrimToNull: " + trim);
        Boolean igual = StringUtils.equals(a,b);
        System.out.println("Resultado equals: " + igual);
        Boolean contem = StringUtils.contains(d, e);
        System.out.println("Resultado Contains: " + contem);
        
        
    }
}
