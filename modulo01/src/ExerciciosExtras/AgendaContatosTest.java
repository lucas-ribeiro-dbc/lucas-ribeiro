import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest {
    @Test
    public void adicionarContatoEPesquisalo() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        String resultado = agenda.consultar("Bernardo");
        assertEquals("555555", resultado);
    }

    @Test
    public void adicionarDoisContatosEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Uthred", "222222");
        String separador = System.lineSeparator();
        String esperado = String.format("Bernardo,555555%sUthred,222222%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
}
