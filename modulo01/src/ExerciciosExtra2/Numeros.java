public class Numeros{
    double[] numeros;
    public Numeros(double[] numeros){
        this.numeros = numeros;
    }
    public double[] calcularMediaSeguinte(){
        if(numeros.length == 0){
            return null;
        }
        double[] resultado = new double[numeros.length-1];
        int indice = 0;
      
        for(int i = 0 ; i < numeros.length-1; i++){
            resultado[indice] = (numeros[i] + numeros[i+1])/2;
            indice++;
            
        }
        return resultado;
    }
}