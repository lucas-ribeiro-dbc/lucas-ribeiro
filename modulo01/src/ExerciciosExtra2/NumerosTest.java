

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class NumerosTest
{
    @Test
    public void verificaCalcularMediaSeguinte(){
        double[] entrada = new double[ ] { 1.0, 3.0, 5.0, 1.0, -10.0 };
        double[] resultado = new double[] { 2.0, 4.0, 3.0, -4.5};
	Numeros numeros = new Numeros(entrada);
	assertArrayEquals(resultado, numeros.calcularMediaSeguinte(), 0.1);   
    }
    @Test
    public void verificaCalcularMediaSeguinteArrayVazio(){
        double[] entrada = new double[ ] {0, 0 ,0 ,0 ,0};
        double[] resultado = new double[] {0, 0 ,0 ,0};
	Numeros numeros = new Numeros(entrada);
	assertArrayEquals(resultado, numeros.calcularMediaSeguinte(), 0.1);   
    }
    @Test
    public void verificaCalcularMediaSeguinteArrayNull(){
        double[] entrada = new double[ ] {};
        double[] resultado = new double[] {};
	Numeros numeros = new Numeros(entrada);
	assertArrayEquals(null, numeros.calcularMediaSeguinte(), 0.1);   
    }
}
