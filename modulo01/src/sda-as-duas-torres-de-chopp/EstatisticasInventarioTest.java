<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
<<<<<<< HEAD
import java.util.ArrayList;
/**
 * A classe de teste EstatisticasInventarioTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class EstatisticasInventarioTest
{
    private final double DELTA = 0.1;
    @After
    public void teardDown(){
        System.gc();
    }
    @Test
    public void testarMediaInventarioCheio(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        Item espada = new Item("Espada de aço valiriano", 20);
        Item escudo = new Item("Escudo de madeira", 30);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(20, estatisticas.getMedia(), DELTA);
    }
    @Test
    public void testarMediaInventarioVazio(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0, estatisticas.getMedia(), DELTA);
    }
    @Test
    public void testarMediaInventarioComUmItem(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        inventario.adicionar(lanca);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(10, estatisticas.getMedia(), DELTA);
    }
    @Test
    public void testarMedianaInventarioCheioPar(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        Item espada = new Item("Espada de aço valiriano", 20);
        Item escudo = new Item("Escudo de madeira", 30);
        Item flechas = new Item("Flechas", 40);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(25.0, estatisticas.getMediana(), DELTA);
    }
    @Test
    public void testarMedianaInventarioCheioImpar(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        Item espada = new Item("Espada de aço valiriano", 20);
        Item escudo = new Item("Escudo de madeira", 30);
        Item flechas = new Item("Flechas", 40);
        Item botas = new Item("Botas 7 leguas", 5);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(botas);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(20, estatisticas.getMediana(), DELTA);
    }
    @Test
    public void testarMedianaInventarioComUmItem(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        inventario.adicionar(lanca);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(10, estatisticas.getMediana(), DELTA);
    }
    @Test
    public void testarMedianaInventarioComDoisItens(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        Item escudo = new Item("Escudo de madeira", 30);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(20, estatisticas.getMediana(), DELTA);
    }
     @Test
    public void testarMedianaInventarioVazio(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0, estatisticas.getMediana(), DELTA);
    }
    @Test
    public void testarQtdItensAcimaDaMediaInventarioCheio(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        Item espada = new Item("Espada de aço valiriano", 20);
        Item escudo = new Item("Escudo de madeira", 30);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(1, estatisticas.qtdItensAcimaDaMedia());
    }
    @Test
    public void testarQtdItensAcimaDaMediaInventarioVazio(){
=======

public class EstatisticasInventarioTest {

    private final double DELTA = 0.1;

    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMedia()));
        // assertNull(estatisticas.calcularMedia());
    }

    @Test
    public void calcularMediaInventarioUmElemento() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2, estatisticas.calcularMedia(), DELTA);
    }

    @Test
    public void calcularMediaInventarioQtdsIguais() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 4));
        inventario.adicionar(new Item("Flechas de fogo", 4));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(4, estatisticas.calcularMedia(), DELTA);
    }

    @Test
    public void calcularMediaInventarioQtdsDiferentes() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 4));
        inventario.adicionar(new Item("Flechas de fogo", 4));
        inventario.adicionar(new Item("Anduril", 1));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMedia(), DELTA);
    }

    @Test
    public void calcularMedianaComInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMediana()));
        // assertNull(estatisticas.calcularMediana());
    }

    @Test
    public void calcularMedianaComUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Lembas", 5));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(5, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void calcularMedianaComQtdParDeItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Lembas", 5));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3.5, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void calcularMedianaComQtdImparDeItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Lembas", 5));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void calcularMedianaComItensDesordenados() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Lembas", 5));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void qtdItensAcimaDaMediaInventarioVazio() {
>>>>>>> master
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }
<<<<<<< HEAD
    @Test
    public void testarQtdItensAcimaDaMediaInventarioComUmItem(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 10);
        inventario.adicionar(lanca);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }
=======

    @Test
    public void qtdItensAcimaDaMediaComUmItem() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item("Arco de vidro", 1));
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComVariosItens() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item("Lembas", 5));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Flechas de fogo", 2));
        inventario.adicionar(new Item("Clava de madeira", 6));
        assertEquals(3, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComItemIgualAMedia() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item("Lembas", 5));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
>>>>>>> master
}
