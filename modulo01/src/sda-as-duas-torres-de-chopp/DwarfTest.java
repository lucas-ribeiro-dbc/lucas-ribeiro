<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

<<<<<<< HEAD
/**
 * The test class DwarfTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class DwarfTest
{
    private final double DELTA = 0.1;
    @After
    public void teardDown(){
        System.gc();
    }
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf carlinhos = new Dwarf("Carlinhos");
        carlinhos.diminuiVida();
        assertEquals(100.0, carlinhos.getVida(), DELTA);
    }
    @Test
    public void dwarfNasceComStatusVivo(){
        Dwarf carlinhos = new Dwarf("Carlinhos");
        assertEquals(Status.VIVO, carlinhos.getStatus());
    }
    @Test
    public void dwarfPerde110DeVidaEMorre(){
        Dwarf carlinhos = new Dwarf("Carlinhos");
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        assertEquals(Status.MORTO, carlinhos.getStatus());
    }
    @Test
    public void dwarfNaoPerdeVidaDepoisDeMorto(){
        Dwarf carlinhos = new Dwarf("Carlinhos");
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        carlinhos.diminuiVida();
        assertEquals(Status.MORTO, carlinhos.getStatus());
        assertEquals(0, carlinhos.getVida(), DELTA);
    }
}
=======
public class DwarfTest {

    private final double DELTA = 0.1;

    @Test
    public void dwarfNasceComStatusVivoE110Vida() {
        Dwarf dwarf = new Dwarf("Carlota Joaquina de Bourbon, filha de Carlos IV de Espanha");
        assertEquals(110.0, dwarf.getVida(), DELTA);
        assertEquals(Status.VIVO, dwarf.getStatus());
    }    

    @Test
    public void dwarfPerde10DeVida() {
        Dwarf dwarf = new Dwarf("Bernardin");
        dwarf.perderVida();
        assertEquals(100.0, dwarf.getVida(), DELTA);
        assertEquals(Status.VIVO, dwarf.getStatus());
    }

    @Test
    public void dwarfPerde110DeVida() {
        Dwarf dwarf = new Dwarf("Thorin Escudo de Carvalho");
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        assertEquals(0.0, dwarf.getVida(), DELTA);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }

    @Test
    public void dwarfPerderVidaMaisQue11Vezes() {
        Dwarf dwarf = new Dwarf("Thorin Escudo de Carvalho");
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        assertEquals(0.0, dwarf.getVida(), DELTA);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
}
>>>>>>> master
