import java.util.ArrayList;
<<<<<<< HEAD
public class PaginadorInventario{
    private Inventario inventario;
    private int marcador;
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    public void pular(int valor){
        if(valor >= 0 && valor < inventario.size()) this.marcador = valor;
    }
    public ArrayList limitar(int valor){
        if(valor > inventario.size() || valor <= 0)return null;
        ArrayList<Item> itens = new ArrayList<>();
        for(int i = marcador ; i < valor+marcador; i++){
            itens.add(inventario.obter(i));
        }
        return itens;
    }
    public int getMarcador(){
        return this.marcador;
=======

public class PaginadorInventario {
    private Inventario inventario;
    private int marcador;

    public PaginadorInventario(Inventario inventario) {
        this.inventario = inventario;
        this.marcador = 0;
    }
    
    public void pular(int marcador) {
        this.marcador = marcador;
    }

    public ArrayList<Item> limitar(int qtd) {
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + qtd;
        for (int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++) {
            subConjunto.add(this.inventario.obter(i));
        }
        return subConjunto;
>>>>>>> master
    }
}