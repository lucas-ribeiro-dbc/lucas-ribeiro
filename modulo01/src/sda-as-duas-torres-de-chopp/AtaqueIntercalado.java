import java.util.*;

public class AtaqueIntercalado implements EstrategiaDeAtaque{
   public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes){
    List<Elfo> ordemDeAtaque = new ArrayList<>();
    for(int i=0; i < atacantes.size(); i++){
        if(atacantes.get(i) instanceof ElfoVerde && !ordemDeAtaque.contains(atacantes.get(i))){
          ordemDeAtaque.add(atacantes.get(i));
          for(int j=0; j < atacantes.size(); j++){
           if(atacantes.get(j) instanceof ElfoNoturno && !ordemDeAtaque.contains(atacantes.get(j))){
            ordemDeAtaque.add(atacantes.get(j));
            break;
            }
          } 
        }
        
        if(atacantes.get(i) instanceof ElfoNoturno && !ordemDeAtaque.contains(atacantes.get(i))){
          ordemDeAtaque.add(atacantes.get(i));
          for(int j=0; j < atacantes.size(); j++){
           if(atacantes.get(j) instanceof ElfoVerde && !ordemDeAtaque.contains(atacantes.get(j))){
            ordemDeAtaque.add(atacantes.get(j));
            break;
            }
          } 
        }
    }
    return ordemDeAtaque;
    }

}
