

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class NoturnosPorUltimoTest{
   @Test
   public void elfosVerdesDevemVirPrimeiro(){
       NoturnosPorUltimo estrategia = new NoturnosPorUltimo();
       List<Elfo> resultado = new ArrayList<>();
       List<Elfo> exercitoAoContrario = new ArrayList<>();
       
       Elfo legolasNoturno = new ElfoNoturno("DarkLegolas");
       Elfo legolas = new ElfoVerde("Legolas");
       
       resultado.add(legolas);
       resultado.add(legolasNoturno);
       
       exercitoAoContrario.add(legolasNoturno);
       exercitoAoContrario.add(legolas);
       
       List<Elfo> exercito = estrategia.getOrdemDeAtaque(exercitoAoContrario);
       
       assertEquals(resultado, exercito);
     
    }
}
