

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test
{
    @Test
    public void testarSortear(){
        DadoD6 dado = new DadoD6();
        int valor = dado.sortear();
        assertTrue((valor > 0 && valor <= 6));
    }  
}
