import java.util.*;
public class NoturnosPorUltimo implements EstrategiaDeAtaque{
   public List<Elfo> getOrdemDeAtaque(List<Elfo> atacantes){
    List<Elfo> ordemDeAtaque = new ArrayList<>();
    for(Elfo elfoAtacante:atacantes){
        if(elfoAtacante.getStatus() == Status.VIVO && elfoAtacante instanceof ElfoVerde){
            ordemDeAtaque.add(elfoAtacante);
        }
    }
      for(Elfo elfoAtacante:atacantes){
        if(elfoAtacante.getStatus() == Status.VIVO && elfoAtacante instanceof ElfoNoturno){
            ordemDeAtaque.add(elfoAtacante);
        }
    }
    return ordemDeAtaque;
    }
}
