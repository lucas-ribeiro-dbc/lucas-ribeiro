<<<<<<< HEAD
public class EstatisticasInventario{
    private Inventario inventario;
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    public double getMedia(){
        if(inventario.size() == 0) return 0;
        double resultado = 0;
        for(int i = 0; i < inventario.size(); i++){
            resultado += inventario.obter(i).getQuantidade();
        }
        return resultado/inventario.size();
    }
    public double getMediana(){
        if(inventario.size() == 0) return 0;
        inventario.ordenar();
        if(inventario.size() % 2 == 0){
           double mediana = inventario.obter(inventario.size()/2).getQuantidade() + inventario.obter((inventario.size()/2)-1).getQuantidade();
           return mediana/2;
        }
        return inventario.obter(inventario.size()/2).getQuantidade();
    }
    public int qtdItensAcimaDaMedia(){
        double media = getMedia();
        if(inventario.size() == 0 || media == 0)return 0;
        int resultado = 0;
        for(int i = 0; i < inventario.size(); i++){
            if(inventario.obter(i).getQuantidade() > media){
                resultado++;
            }        
        }
        return resultado;
    }
}
=======
public class EstatisticasInventario {

    private Inventario inventario;

    public EstatisticasInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public double calcularMedia() {
        if (this.inventario.vazio()) {
            return Double.NaN;
        }
        double somaQtds = .0;
        for (Item item : inventario.getItens()) {
            somaQtds += item.getQuantidade();
        }
        return somaQtds / inventario.getItens().size();
    }

    public double calcularMediana() {
        if (this.inventario.vazio()) {
            return Double.NaN;
        }
        this.inventario.ordenarItens();
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        boolean qtdImpar = qtdItens % 2 == 1;
        if (qtdImpar) {
            return this.inventario.obter(meio).getQuantidade();
        }
        int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        return (qtdMeioMenosUm + qtdMeio) / 2.;
    }
    
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int qtdAcima = 0;
        
        for (Item item : this.inventario.getItens()) {
            if (item.getQuantidade() > media) {
                qtdAcima++;
            }
        }
        return qtdAcima;
    }
}
>>>>>>> master
