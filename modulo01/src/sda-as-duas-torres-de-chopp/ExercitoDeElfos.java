import java.util.*;
<<<<<<< HEAD
public class ExercitoDeElfos
{
    private HashMap<Status, ArrayList<Elfo>> alistados = new HashMap<>(); 
    private static final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(Arrays.asList(ElfoVerde.class, ElfoNoturno.class));
    public void alistarElfo(Elfo elfo){
        if(TIPOS_PERMITIDOS.contains(elfo.getClass())){
        ArrayList<Elfo> listaAtualDeElfos = buscarPorStatus(elfo.getStatus());
        boolean primeiroElfoAlistado = listaAtualDeElfos == null;
        if (primeiroElfoAlistado) {
            listaAtualDeElfos = new ArrayList<>();
            alistados.put(elfo.getStatus(), listaAtualDeElfos);
        }
        listaAtualDeElfos.add(elfo);
      }
    }   
    public ArrayList<Elfo> buscarPorStatus(Status status){
        return this.alistados.get(status);
    }
    public HashMap<Status, ArrayList<Elfo>> getAlistados(){
        return alistados;
    }
    public void atualizarExercito(){
        ArrayList<Elfo> elfosAlistadosVivos = buscarPorStatus(Status.VIVO);
        if(elfosAlistadosVivos != null){
            for(int i = 0; i< elfosAlistadosVivos.size(); i++){
                if(elfosAlistadosVivos.get(i).getStatus() == Status.MORTO){
                    alistarElfo(elfosAlistadosVivos.get(i));
                    elfosAlistadosVivos.remove(i);
                }         
            }
        }    
    }
}
=======

public class ExercitoDeElfos {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class
            )
        );
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); 

    public void alistar(Elfo elfo) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if (podeAlistar) {
            elfos.add(elfo);
            ArrayList<Elfo> elfosDoStatus = porStatus.get(elfo.getStatus());
            if (elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> buscar(Status status) {
        return this.porStatus.get(status);
    }

    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
}

>>>>>>> master
