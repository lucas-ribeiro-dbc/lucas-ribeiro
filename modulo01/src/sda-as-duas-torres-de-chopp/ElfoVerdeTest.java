<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
<<<<<<< HEAD
public class ElfoVerdeTest
{
    @After
    public void teardDown(){
        System.gc();
    }
    @Test
    public void verificaElfoVerdeNascendoComItens(){
        ElfoVerde ev = new ElfoVerde("Galadriel");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Arco",1));
        itens.add(new Item("Flecha",7));
        assertEquals("Galadriel", ev.getNome());
        assertEquals(itens, ev.getInventario().getItens());
    }
    @Test
    public void ganharItemDeTipoDiferente(){
        ElfoVerde ev = new ElfoVerde("Galadriel");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Arco",1));
        itens.add(new Item("Flecha",7));
        itens.add(new Item("Arco de Madeira",1));
        ev.ganharItem(new Item("Arco de Madeira",1));
        assertNotSame(itens, ev.getInventario().getItens()); 
    }
    @Test
    public void ganharItemDeTipoVidro(){
        ElfoVerde ev = new ElfoVerde("Galadriel");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Arco",1));
        itens.add(new Item("Flecha",7));
        itens.add(new Item("Arco de Vidro",1));
        ev.ganharItem(new Item("Arco de Vidro",1));
        assertEquals(itens, ev.getInventario().getItens()); 
    }
    @Test
    public void perderItemDeTipoVidro(){
        ElfoVerde ev = new ElfoVerde("Galadriel");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Arco",1));
        itens.add(new Item("Flecha",7));
        ev.ganharItem(new Item("Arco de Vidro",1));
        ev.perderItem(new Item("Arco de Vidro",1));
        assertEquals(itens, ev.getInventario().getItens());
    }
    @Test
    public void atirarFlechaVerificarExperiencia(){
        ElfoVerde ev = new ElfoVerde("Galadriel");
        Dwarf carlinhos = new Dwarf("Carlinhos");
        ev.atirarFlecha(carlinhos);
        assertEquals(2, ev.getExperiencia());
    }
    @Test
    public void atirarFlechaVerificarQuantidadeDeFlechas(){
        ElfoVerde ev = new ElfoVerde("Galadriel");
        Dwarf carlinhos = new Dwarf("Carlinhos");
        ev.atirarFlecha(carlinhos);
        assertEquals(6, ev.getFlecha().getQuantidade());
    }

}
=======
import java.util.Arrays;

public class ElfoVerdeTest {
    
    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void elfoVerdeAdicionarItemPermitido() {
        ElfoVerde elfa = new ElfoVerde("Zaleria Qiric");
        Item arcoVidro = new Item("Arco de Vidro", 1);
        elfa.ganharItem(arcoVidro);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7),
                    arcoVidro
                ));
        assertEquals(esperado, elfa.getInventario().getItens());
    }

    @Test
    public void elfoVerdeAdicionarItemNaoPermitido() {
        ElfoVerde elfa = new ElfoVerde("Zaleria Qiric");
        Item ironBoots = new Item("Botas de ferro", 1);
        elfa.ganharItem(ironBoots);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7)
                ));
        assertEquals(esperado, elfa.getInventario().getItens());
    }

    @Test
    public void elfoVerdeRemoverItemPermitido() {
        ElfoVerde elfa = new ElfoVerde("Zaleria Qiric");
        Item arcoVidro = new Item("Arco de Vidro", 1);
        elfa.ganharItem(arcoVidro);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7)
                ));
        elfa.perderItem(arcoVidro);
        assertEquals(esperado, elfa.getInventario().getItens());
    }

    @Test
    public void elfoVerdeRemoverItemNaoPermitido() {
        ElfoVerde elfa = new ElfoVerde("Zaleria Qiric");
        Item ironBoots = new Item("Botas de ferro", 1);
        elfa.ganharItem(ironBoots);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7)
                ));
        elfa.perderItem(ironBoots);
        assertEquals(esperado, elfa.getInventario().getItens());
    }

    @Test
    public void celebornAtiraUmaFlecha() {
        Elfo celeborn = new ElfoVerde("Celeborn");
        celeborn.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(6, celeborn.getFlecha().getQuantidade());
        assertEquals(2, celeborn.getExperiencia());
    }

    @Test
    public void legolasTentaAtirarOitoFlechas() {
        Elfo elfo = new ElfoVerde("Legolas");
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(0, elfo.getFlecha().getQuantidade());
        assertEquals(14, elfo.getExperiencia());
    }
}
>>>>>>> master
