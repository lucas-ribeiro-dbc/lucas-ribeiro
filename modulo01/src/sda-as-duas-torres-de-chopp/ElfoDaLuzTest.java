<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
<<<<<<< HEAD
/**
 * A classe de teste ElfoDaLuzTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class ElfoDaLuzTest
{
    private final double DELTA = 0.1;
    @After
    public void teardDown(){
        System.gc();
    }
    @Test
    public void verificaElfoDaLuzComItens(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Arco", 1));
        itens.add(new Item("Flecha", 7));
        itens.add(new Item("Espada de Galvorn", 1));
        assertEquals(itens, feanor.getInventario().getItens());
    }
    @Test
    public void verificarAtacarComEspadaPar(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf farlum = new Dwarf("Farlum");
        feanor.atacarComEspada(farlum);
        feanor.atacarComEspada(farlum);
        assertEquals(89.0, feanor.getVida(), DELTA);      
    }
    @Test
    public void verificarAtacarComEspadaImpar(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf farlum = new Dwarf("Farlum");
        feanor.atacarComEspada(farlum);
        assertEquals(79.0, feanor.getVida(), DELTA);      
    }
    @Test
    public void verificarAtacarComEspadaAteMorrer(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf farlum = new Dwarf("Farlum");
        feanor.atacarComEspada(farlum); //79
        feanor.atacarComEspada(farlum); //89
        feanor.atacarComEspada(farlum); //68
        feanor.atacarComEspada(farlum); //78
        feanor.atacarComEspada(farlum); //57
        feanor.atacarComEspada(farlum); //67
        feanor.atacarComEspada(farlum); //46
        feanor.atacarComEspada(farlum); //56
        feanor.atacarComEspada(farlum); //35
        feanor.atacarComEspada(farlum); //45
        feanor.atacarComEspada(farlum); //24
        feanor.atacarComEspada(farlum); //34
        feanor.atacarComEspada(farlum); //13
        feanor.atacarComEspada(farlum); //23
        feanor.atacarComEspada(farlum); //2
        feanor.atacarComEspada(farlum); //12
        feanor.atacarComEspada(farlum); //12
        assertEquals(0, feanor.getVida(), DELTA);   
        assertEquals(Status.MORTO, feanor.getStatus());   
    }
    @Test
    public void verificarQuantidadeDeEspada(){
         ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
         feanor.getInventario().buscar("Espada de Galvorn").setQuantidade(0);
         assertEquals(1, feanor.getEspada().getQuantidade());
    }
    @Test
    public void verificaNaoPodePerderEspadaGalvorn(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Arco", 1));
        itens.add(new Item("Flecha", 7));
        itens.add(new Item("Espada de Galvorn", 1));
        feanor.perderItem(new Item("Espada de Galvorn",1));
        assertEquals(itens, feanor.getInventario().getItens());       
    }
    @Test
    public void verificaNaoPodePerderItem(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        ArrayList<Item> itens = new ArrayList<>();
        itens.add(new Item("Flecha", 7));
        itens.add(new Item("Espada de Galvorn", 1));
        feanor.perderItem(new Item("Arco",1));
        assertEquals(itens, feanor.getInventario().getItens());       
    }
     @Test
    public void atacaComUnidadeDeEspada(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.getInventario().getItens().get(2).setQuantidade(0);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(79, feanor.getVida(), DELTA);      
    }
   
}
=======

public class ElfoDaLuzTest {

    private static final double DELTA = 0.1;

    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void elfoDaLuzNasceComEspada() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzDevePerderVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        //ElfoDaLuz feanorDaLuz = (ElfoDaLuz)feanor;
        //((ElfoDaLuz)feanor).atacarComEspada(new Dwarf(""));
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(79, feanor.getVida(), DELTA);
        assertEquals(100, gul.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzDeveGanharVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        //ElfoDaLuz feanorDaLuz = (ElfoDaLuz)feanor;
        //((ElfoDaLuz)feanor).atacarComEspada(new Dwarf(""));
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89, feanor.getVida(), DELTA);
        assertEquals(90, gul.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzMorreAoAtacarTantasVezes() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        for (int i = 0; i < 17; i++) {
            feanor.atacarComEspada(gul);
        }
        assertEquals(0, feanor.getVida(), DELTA);
        assertEquals(0, gul.getVida(), DELTA);
        assertTrue(feanor.estaMorto());
        assertTrue(gul.estaMorto());
    }

    @Test
    public void elfoDaLuzNaoPodePerderEspadaDeGalvorn() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item("Espada de Galvorn", 1));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzPodePerderArco() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item("Arco", 1));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzSoAtacaComUnidadeDeEspada() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().getItens().get(2).setQuantidade(0);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(79, feanor.getVida(), DELTA);
    }
}






>>>>>>> master
