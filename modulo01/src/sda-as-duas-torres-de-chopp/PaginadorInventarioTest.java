<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

<<<<<<< HEAD
public class PaginadorInventarioTest
{
        @After
    public void teardDown(){
        System.gc();
    }
        @Test
        public void testarPular(){
            Inventario inventario = new Inventario();
            inventario.adicionar(new Item("Espada", 1));
            inventario.adicionar(new Item("Escudo de metal", 2));
            inventario.adicionar(new Item("Poção de HP", 3));
            inventario.adicionar(new Item("Bracelete", 1));
            PaginadorInventario paginador = new PaginadorInventario(inventario);
            paginador.pular(2);
            assertEquals(2, paginador.getMarcador());
        }
        @Test
        public void testarPularValorMaiorQueInventario(){
            Inventario inventario = new Inventario();
            inventario.adicionar(new Item("Espada", 1));
            PaginadorInventario paginador = new PaginadorInventario(inventario);
            paginador.pular(2);
            assertEquals(0, paginador.getMarcador());
        }
        @Test
        public void testarLimitar(){
            Inventario inventario = new Inventario();
            ArrayList<Item> resultado = new ArrayList<>();
            Item espada = new Item("Espada", 1);
            Item escudo = new Item("Escudo de metal", 2);
            inventario.adicionar(espada);
            inventario.adicionar(escudo);
            resultado.add(espada);
            resultado.add(escudo);
            PaginadorInventario paginador = new PaginadorInventario(inventario);
            paginador.pular(0);
            assertEquals(resultado, paginador.limitar(2));
        }
          @Test
        public void testarLimitarPularMarcadorDois(){
            Inventario inventario = new Inventario();
            ArrayList<Item> resultado = new ArrayList<>();
            Item espada = new Item("Espada", 1);
            Item escudo = new Item("Escudo de metal", 2);
            Item pocao = new Item("Pocao de hp", 3);
            Item bracelete = new Item("Bracelete",5);
            inventario.adicionar(espada);
            inventario.adicionar(escudo);
            inventario.adicionar(pocao);
            inventario.adicionar(bracelete);
            resultado.add(pocao);
            resultado.add(bracelete);
            PaginadorInventario paginador = new PaginadorInventario(inventario);
            paginador.pular(2);
            assertEquals(resultado, paginador.limitar(2));
        }
         @Test
        public void testarLimitarMarcadorMaiorQueInventario(){
            Inventario inventario = new Inventario();
            ArrayList<Item> resultado = new ArrayList<>();
            Item espada = new Item("Espada", 1);
            Item escudo = new Item("Escudo de metal", 2);
            Item pocao = new Item("Pocao de hp", 3);
            Item bracelete = new Item("Bracelete",5);
            inventario.adicionar(espada);
            inventario.adicionar(escudo);
            inventario.adicionar(pocao);
            inventario.adicionar(bracelete);
            resultado.add(pocao);
            resultado.add(bracelete);
            PaginadorInventario paginador = new PaginadorInventario(inventario);
            paginador.pular(2);
            assertNull(paginador.limitar(5));
        }
        @Test
        public void testarLimitarMarcadorIntermediario(){
            Inventario inventario = new Inventario();
            ArrayList<Item> resultado = new ArrayList<>();
            Item espada = new Item("Espada", 1);
            Item escudo = new Item("Escudo de metal", 2);
            Item pocao = new Item("Pocao de hp", 3);
            Item bracelete = new Item("Bracelete",5);
            inventario.adicionar(espada);
            inventario.adicionar(escudo);
            inventario.adicionar(pocao);
            inventario.adicionar(bracelete);
            resultado.add(escudo);
            resultado.add(pocao);
            resultado.add(bracelete);
            PaginadorInventario paginador = new PaginadorInventario(inventario);
            paginador.pular(1);
            assertEquals(resultado, paginador.limitar(3));
        }
=======
public class PaginadorInventarioTest {

    @Test
    public void pularLimitarComInventarioVazio() {
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertTrue(primeiraPagina.isEmpty());
    }

    @Test
    public void pularLimitarComApenasUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada de ferro", 2);
        inventario.adicionar(espada);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> resultado = paginador.limitar(1);
        assertEquals(espada, resultado.get(0));
        assertEquals(1, resultado.size());
    }

    @Test
    public void pularLimitarDentroDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudoMetal = new Item("Escudo de metal", 2);
        Item pocaoHp = new Item("Poção de HP", 3);
        Item bracelete = new Item("Bracelete", 1);
        inventario.adicionar(espada);
        inventario.adicionar(escudoMetal);
        inventario.adicionar(pocaoHp);
        inventario.adicionar(bracelete);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        assertEquals(2, primeiraPagina.size());
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudoMetal, primeiraPagina.get(1));
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(2);
        assertEquals(2, segundaPagina.size());
        assertEquals(pocaoHp, segundaPagina.get(0));
        assertEquals(bracelete, segundaPagina.get(1));
    }

    @Test
    public void pularForaDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudoMetal = new Item("Escudo de metal", 2);
        inventario.adicionar(espada);
        inventario.adicionar(escudoMetal);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(3);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        assertTrue(primeiraPagina.isEmpty());
    }

    @Test
    public void limitarForaDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudoMetal = new Item("Escudo de metal", 2);
        inventario.adicionar(espada);
        inventario.adicionar(escudoMetal);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(3);
        assertEquals(2, primeiraPagina.size());
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudoMetal, primeiraPagina.get(1));
    }
    
    @Test
    public void pularELimitarForaDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudoMetal = new Item("Escudo de metal", 2);
        inventario.adicionar(espada);
        inventario.adicionar(escudoMetal);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(3);
        ArrayList<Item> primeiraPagina = paginador.limitar(3);
        assertTrue(primeiraPagina.isEmpty());
    }

>>>>>>> master
}
