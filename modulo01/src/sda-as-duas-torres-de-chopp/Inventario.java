import java.util.ArrayList;
<<<<<<< HEAD

public class Inventario {
    private ArrayList<Item> itens;
    private int posicaoAPreencher = 0, itensPreenchidos = 0;
    
    public Inventario(){
        itens = new ArrayList<>();
    }
    public void adicionar(Item item) {
       this.itens.add(item);
    }

    public Item obter(int posicao) {
        if(itens.isEmpty()) return null;
=======
import java.util.Collections;
import java.util.Comparator;

public class Inventario {
    private ArrayList<Item> itens;

    public Inventario() {
        this(0);
    }

    public Inventario(int quantidadeDeItens) {
        this.itens = new ArrayList<>(quantidadeDeItens);
    }

    public ArrayList<Item> getItens() {
        return this.itens;
    }

    public void adicionar(Item item) {
        this.itens.add(item);
    }

    public Item obter(int posicao) {
>>>>>>> master
        return this.itens.get(posicao);
    }

    public void remover(int posicao) {
        this.itens.remove(posicao);
    }

<<<<<<< HEAD
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder(1024);
=======
    public boolean vazio() {
        return this.itens.isEmpty();
    }

    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
>>>>>>> master
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i) != null) {
                String descricao = this.itens.get(i).getDescricao();
                descricoes.append(descricao);
<<<<<<< HEAD
                if (i < this.size() - 1) {
=======
                boolean deveColocarVirgula = i < this.itens.size() - 1;
                if (deveColocarVirgula) {
>>>>>>> master
                    descricoes.append(",");
                }
            }
        }
<<<<<<< HEAD
=======

>>>>>>> master
        return descricoes.toString();
    }

    public Item getItemComMaiorQuantidade() {
<<<<<<< HEAD
=======

>>>>>>> master
        int indice = 0, maiorQuantidadeParcial = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i) != null) {
                int qtdAtual = this.itens.get(i).getQuantidade();
                if (qtdAtual > maiorQuantidadeParcial) {
                    maiorQuantidadeParcial = qtdAtual;
                    indice = i;
                }
            }
        }
<<<<<<< HEAD
        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }
    public Item buscar(String descricao){
        for(Item i: itens){
           if(i.getDescricao().equalsIgnoreCase(descricao)){
           return i;
        }
        }
        return null;
    }
    public ArrayList<Item> inverter(){
        if(itens.isEmpty()) return null;
        ArrayList<Item> itensInvertidos = new ArrayList<>();
        for(int i = itens.size()-1; i >= 0; i--){
            itensInvertidos.add(itens.get(i));
        }
        return itensInvertidos;
    }
    public void ordenar(){
        if(itens.isEmpty()) return;
        Item auxiliar;
        for(int i = 0; i < itens.size(); i++){
            for(int j = 0; j < itens.size()-1; j++){
                if(itens.get(j).getQuantidade() > itens.get(j+1).getQuantidade()){
                    auxiliar = itens.get(j);
                    itens.set(j, itens.get(j+1));
                    itens.set(j+1, auxiliar);
                }
            }
        }
    }   
    public void ordenar(TipoOrdenacao tipo){
       if(itens.isEmpty()) return;
        if(tipo == TipoOrdenacao.ASC){
            this.ordenar();
        }else{
        Item auxiliar;
        for(int i = 0; i < itens.size(); i++){
            for(int j = 0; j < itens.size()-1; j++){
                if(itens.get(j).getQuantidade() < itens.get(j+1).getQuantidade()){
                    auxiliar = itens.get(j);
                    itens.set(j, itens.get(j+1));
                    itens.set(j+1, auxiliar);
                }
            }
        }
       }
    }   
    public int size(){
        return this.itens.size();
    }
    public ArrayList<Item> getItens(){
        return itens;
=======
        return !this.itens.isEmpty() ? this.itens.get(indice) : null;
    }

    public Item buscar(String descricao) {
        for (int i = 0; i < this.itens.size(); i++) {
            Item itemAtual = this.itens.get(i);
            if (itemAtual.getDescricao().equals(descricao)) {
                return itemAtual;
            }
        }
        return null;
    }

    public ArrayList<Item> inverter() {
        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
        for (int i = this.itens.size() - 1; i >= 0; i--) {
            listaInvertida.add(this.itens.get(i));
        }
        return listaInvertida;
    }

    public void ordenarItens() {
        ordenarItens(TipoOrdenacao.ASC);
    }

    public void ordenarItens(TipoOrdenacao tipoOrdenacao) {
        Collections.sort(this.itens, new Comparator<Item>() {
                public int compare(Item item1, Item item2) {
                    int quantidade1 = item1.getQuantidade();
                    int quantidade2 = item2.getQuantidade();
                    return tipoOrdenacao == TipoOrdenacao.ASC ?
                        Integer.compare(quantidade1, quantidade2) :
                        Integer.compare(quantidade2, quantidade1);
                }
            });
>>>>>>> master
    }
}