

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class AtaqueIntercaladoTest
{
   @Test
   public void ordemIntercalada(){
       AtaqueIntercalado estrategia = new AtaqueIntercalado();
       
       List<Elfo> resultado = new ArrayList<>();
       List<Elfo> exercito = new ArrayList<>();
       
       Elfo legolasNoturno = new ElfoNoturno("DarkLegolas");
       Elfo legolas = new ElfoVerde("Legolas");
       Elfo winningNoturno = new ElfoNoturno("WinningLegolas");
       Elfo winning = new ElfoVerde("Winning");
       
       resultado.add(legolasNoturno);
       resultado.add(legolas);
       resultado.add(winningNoturno);
       resultado.add(winning);
       
       exercito.add(legolasNoturno);
       exercito.add(winningNoturno);
       exercito.add(legolas);
       exercito.add(winning);
       
       
       List<Elfo> exercitoOrdenado = estrategia.getOrdemDeAtaque(exercito);
       
       assertEquals(resultado, exercitoOrdenado);
     
    }
}
