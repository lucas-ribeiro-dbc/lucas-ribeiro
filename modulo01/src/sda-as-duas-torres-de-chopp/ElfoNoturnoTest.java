<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

<<<<<<< HEAD
/**
 * A classe de teste ElfoNoturnoTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class ElfoNoturnoTest
{
    private final double DELTA = 0.1;
    @After
    public void teardDown(){
        System.gc();
    }
    @Test
    public void verificaNome(){
        ElfoNoturno legolas = new ElfoNoturno("Legolas");
        assertEquals("Legolas", legolas.getNome());
    }
    @Test
    public void verificaAtirarUmaFlecha(){
        ElfoNoturno legolas = new ElfoNoturno("Legolas");
        legolas.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(6, legolas.getFlecha().getQuantidade());
        assertEquals(3, legolas.getExperiencia());
        assertEquals(85.0, legolas.getVida(), DELTA);
    }
     @Test
    public void verificaAtirarOitoFlechas(){
        ElfoNoturno legolas = new ElfoNoturno("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        legolas.atirarFlecha(dwarf);
        assertEquals(0, legolas.getVida(), DELTA);
        assertEquals(21, legolas.getExperiencia());
    }
}
=======
public class ElfoNoturnoTest {

    private final double DELTA = 0.1;

    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void elfoNoturnoNaoAtirarFlechaNaoMudaVida() {
        Elfo aimon = new ElfoNoturno("Aimon Yesfaren");
        assertEquals(100.0, aimon.getVida(), DELTA);
        assertEquals(0, aimon.getExperiencia());
        assertEquals(7, aimon.getFlecha().getQuantidade());
        assertEquals(Status.VIVO, aimon.getStatus());
    }

    @Test
    public void elfoNoturnoAtirarUmaFlecha() {
        Elfo aimon = new ElfoNoturno("Aimon Yesfaren");
        aimon.atirarFlecha(new Dwarf("Thorin"));
        assertEquals(85., aimon.getVida(), DELTA);
        assertEquals(3, aimon.getExperiencia());
        assertEquals(6, aimon.getFlecha().getQuantidade());
        assertEquals(Status.VIVO, aimon.getStatus());
    }

    @Test
    public void elfoNoturnoAtirarSeteFlechas() {
        Elfo aimon = new ElfoNoturno("Aimon Yesfaren");
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        assertEquals(0., aimon.getVida(), DELTA);
        assertEquals(21, aimon.getExperiencia());
        assertEquals(0, aimon.getFlecha().getQuantidade());
        assertEquals(Status.MORTO, aimon.getStatus());
    }

    @Test
    public void elfoNoturnoAtirarOitoFlechas() {
        Elfo aimon = new ElfoNoturno("Aimon Yesfaren");
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        aimon.atirarFlecha(new Dwarf("Thorin"));
        assertEquals(0., aimon.getVida(), DELTA);
        assertEquals(21, aimon.getExperiencia());
        assertEquals(0, aimon.getFlecha().getQuantidade());
        assertEquals(Status.MORTO, aimon.getStatus());
    }
}
>>>>>>> master
