<<<<<<< HEAD


=======
>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

<<<<<<< HEAD
/**
 * The test class ExercitoDeElfosTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ExercitoDeElfosTest
{
    @After
    public void teardDown(){
        System.gc();
    }
    @Test
    public void testarAlistar(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        ArrayList<Elfo> resultado = new ArrayList<>();
        ElfoVerde legolas = new ElfoVerde("Legolas");
        ElfoNoturno darkLegolas = new ElfoNoturno("Dark Legolas");
        ElfoVerde grim = new ElfoVerde("Grim");
        ElfoNoturno darkGrim = new ElfoNoturno("Dark Grim");
        
        resultado.add(legolas);
        resultado.add(darkLegolas);
        resultado.add(grim);
        resultado.add(darkGrim);
        
        exercito.alistarElfo(legolas);
        exercito.alistarElfo(darkLegolas);
        exercito.alistarElfo(grim);
        exercito.alistarElfo(darkGrim);  
        
        assertEquals(resultado, exercito.buscarPorStatus(Status.VIVO));  
        }
    @Test
    public void testarBuscarPorStatusVivo(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        ArrayList<Elfo> resultado = new ArrayList<>();
        ElfoVerde legolas = new ElfoVerde("Legolas");
        ElfoNoturno darkLegolas = new ElfoNoturno("Dark Legolas");
        
        resultado.add(legolas);
        resultado.add(darkLegolas);
     
        exercito.alistarElfo(legolas);
        exercito.alistarElfo(darkLegolas);

        assertEquals(resultado, exercito.buscarPorStatus(Status.VIVO));
        
    }
    @Test
    public void testarBuscarPorStatusMorto(){
        ExercitoDeElfos exercito = new ExercitoDeElfos(); 
        assertEquals(null, exercito.buscarPorStatus(Status.MORTO));
        
    }
     @Test
    public void testarAlistarElfoNormal(){
        ExercitoDeElfos exercito = new ExercitoDeElfos(); 
        Elfo legolas = new Elfo("Legolas");
        exercito.alistarElfo(legolas);
        assertEquals(null, exercito.buscarPorStatus(Status.MORTO));    
    }
    @Test
    public void testarAtualizarExercito(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        ArrayList<Elfo> elfosMortos = new ArrayList<>();
        
        ElfoVerde legolas = new ElfoVerde("Legolas");
        ElfoNoturno darkLegolas = new ElfoNoturno("Dark Legolas");

        elfosMortos.add(legolas);
     
        exercito.alistarElfo(legolas);
        exercito.alistarElfo(darkLegolas);
        
        HashMap<Status, ArrayList<Elfo>> alistados = exercito.getAlistados();
        
        //"Mata" elfo legolas
        alistados.get(Status.VIVO).get(0).setStatus(Status.MORTO);
        
        exercito.atualizarExercito();

        assertEquals(elfosMortos, exercito.buscarPorStatus(Status.MORTO));
        
    }
}
=======
public class ExercitoDeElfosTest {

    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }

    @Test
    public void naoPodeAlistarElfo() {
        Elfo elfo = new Elfo("Common Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfo = new ElfoDaLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void buscarElfosVivosExistindo() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(vivo)
            );
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }

    @Test
    public void buscarElfosVivosNaoExistindo() {
        Elfo vivoTrocaPraMorto = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        vivoTrocaPraMorto.setStatus(Status.MORTO);
        exercito.alistar(vivoTrocaPraMorto);
        assertNull(exercito.buscar(Status.VIVO));
    }

    @Test
    public void buscarElfosMortosExistindo() {
        Elfo morto = new ElfoVerde("Undead Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        morto.setStatus(Status.MORTO);
        exercito.alistar(morto);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(morto)
            );
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }    

    @Test
    public void buscarElfosMortosNaoExistindo() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(vivo);
        assertNull(exercito.buscar(Status.MORTO));
    }
}

>>>>>>> master
