import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
<<<<<<< HEAD

public class InventarioTest {
    @After
    public void teardDown(){
        System.gc();
    }
=======
import java.util.Arrays;

public class InventarioTest {

    @Test
    public void criarInventarioInformandoQuantidadeItens() {
        Inventario inventario = new Inventario(42);
        assertEquals(0, inventario.getItens().size());
    }

>>>>>>> master
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        inventario.adicionar(espada);
<<<<<<< HEAD
        assertEquals(espada, inventario.obter(0));
=======
        assertEquals(espada, inventario.getItens().get(0));
    }

    @Test
    public void adicionarDoisItensComEspaçoInicialParaUmAdicionaSegundo() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item("Espada", 1);
        Item armadura = new Item("Armadura", 1);
        inventario.adicionar(espada);
        inventario.adicionar(armadura);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(2, inventario.getItens().size());
>>>>>>> master
    }

    @Test
    public void obterItemNaPrimeiraPosicao() {
<<<<<<< HEAD
        Inventario inventario = new Inventario();
=======
        Inventario inventario = new Inventario(1);
>>>>>>> master
        Item espada = new Item("Espada", 1);
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }

<<<<<<< HEAD
    @Test
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        inventario.adicionar(espada);
        inventario.remover(0);
        assertNull(inventario.obter(0));
=======
    @Test(expected=IndexOutOfBoundsException.class)
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item("Espada", 1);
        Item primeiroItem = inventario.obter(0);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void removerItem() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item("Espada", 1);
        inventario.adicionar(espada);
        inventario.remover(0);
        Item primeiroItem = inventario.obter(0);
>>>>>>> master
    }

    @Test
    public void removerItemAntesDeAdicionarProximo() {
<<<<<<< HEAD
        Inventario inventario = new Inventario();
=======
        Inventario inventario = new Inventario(1);
>>>>>>> master
        Item espada = new Item("Espada", 1);
        Item armadura = new Item("Armadura", 1);
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
<<<<<<< HEAD
        assertEquals(1, inventario.size());
=======
        assertEquals(1, inventario.getItens().size());
>>>>>>> master
    }

    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudo = new Item("Escudo", 2);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Escudo", resultado);
    }

    @Test
    public void getDescricoesItensRemovendoItemNoMeio() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudo = new Item("Escudo", 2);
        Item flechas = new Item("Flechas", 3);
<<<<<<< HEAD
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.remover(1);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Flechas", resultado);
=======
        Item xicara = new Item("Chip", 1);

        inventario.adicionar(espada);
        inventario.adicionar(escudo);//
        inventario.adicionar(flechas);
        inventario.adicionar(xicara);//
        inventario.adicionar(espada);
        inventario.adicionar(escudo);//
        inventario.adicionar(flechas);
        inventario.adicionar(xicara);//

        inventario.remover(1);
        inventario.remover(2);
        inventario.remover(3);
        inventario.remover(4);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Flechas,Espada,Flechas", resultado);
>>>>>>> master
    }

    @Test
    public void getDescricoesItensVazio() {
        Inventario inventario = new Inventario();
        String resultado = inventario.getDescricoesItens();
        assertEquals("", resultado);
    }

    @Test
    public void getItemMaiorQuantidadeComVarios() {
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 1);
        Item espada = new Item("Espada de aço valiriano", 3);
        Item escudo = new Item("Escudo de madeira", 2);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }
<<<<<<< HEAD
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
        Inventario inventario = new Inventario();
=======

    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
        Inventario inventario = new Inventario(0);
>>>>>>> master
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertNull(resultado);
    }

    @Test
    public void getItemMaiorQuantidadeItensComMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada de aço valiriano", 3);
        Item escudo = new Item("Escudo de madeira", 2);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
<<<<<<< HEAD
    @Test
    public void testarBuscarItemAdicionado(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 3);
        inventario.adicionar(lanca);
        assertEquals(lanca, inventario.buscar("Lança"));
    }
    @Test
    public void testarBuscarItemNaoAdicionado(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 3);
        assertNull(inventario.buscar("Lança"));
    }
    @Test
    public void testarBuscarItemNoMeio(){
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada", 3);
        Item escudo = new Item("Escudo", 3);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.buscar("Espada"));
    }
    @Test
    public void testarGetItens(){
        Inventario inventario = new Inventario();
        ArrayList<Item> itens = new ArrayList<>();
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada", 3);
        Item escudo = new Item("Escudo", 3);
        inventario.adicionar(lanca);
        itens.add(lanca);
        inventario.adicionar(espada);
        itens.add(espada);
        inventario.adicionar(escudo);
        itens.add(escudo);
        assertEquals(itens, inventario.getItens());
    }
    @Test
    public void testarInverterItens(){
        Inventario inventario = new Inventario();
        ArrayList<Item> itens = new ArrayList<>();
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada", 3);
        Item escudo = new Item("Escudo", 3);
        Item flechas = new Item("Flechas", 33);
        itens.add(flechas);
        inventario.adicionar(lanca);
        itens.add(escudo);
        inventario.adicionar(espada);
        itens.add(espada);
        inventario.adicionar(escudo);
        itens.add(lanca);
        inventario.adicionar(flechas);
        assertEquals(itens, inventario.inverter());
    }
    @Test
    public void testarInverterArrayListVazio(){
        Inventario inventario = new Inventario();
        assertNull(inventario.inverter());
    }
    @Test
    public void testarInverterArrayListUnicoItem(){
        Inventario inventario = new Inventario();
        ArrayList<Item> itens = new ArrayList<>();
        Item lanca = new Item("Lança", 3);
        inventario.adicionar(lanca);
        itens.add(lanca);
        assertEquals(itens, inventario.inverter());
    }
    @Test
    public void ordenarInventario() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 9);
        Item escudo = new Item("Escudo", 13);
        Item flechas = new Item("Flechas", 3);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.ordenar();
        assertEquals(flechas, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(escudo, inventario.obter(2));
    }
     @Test
    public void ordenarInventarioVazio() {
        Inventario inventario = new Inventario();
        inventario.ordenar();
        assertEquals(inventario, inventario);
    }
    @Test
    public void ordenarInventarioMaiorNoInicio() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 90);
        Item escudo = new Item("Escudo", 16);
        Item flechas = new Item("Flechas", 13);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.ordenar();
        assertEquals(flechas, inventario.obter(0));
        assertEquals(escudo, inventario.obter(1));
        assertEquals(espada, inventario.obter(2));
    }
    @Test
    public void ordenarInventarioMaiorNoFim() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 90);
        Item escudo = new Item("Escudo", 16);
        Item flechas = new Item("Flechas", 130);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.ordenar();
        assertEquals(escudo, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(flechas, inventario.obter(2));
    }
    @Test
    public void ordenarInventarioASC() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 90);
        Item escudo = new Item("Escudo", 16);
        Item flechas = new Item("Flechas", 130);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.ordenar(TipoOrdenacao.ASC);
        assertEquals(escudo, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(flechas, inventario.obter(2));
    }
    @Test
    public void ordenarInventarioDESC() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 90);
        Item escudo = new Item("Escudo", 16);
        Item flechas = new Item("Flechas", 130);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.ordenar(TipoOrdenacao.DESC);
        assertEquals(flechas, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(escudo, inventario.obter(2));
    }
}
=======

    @Test
    public void buscarItemComInventarioVazio() {
        Inventario inventario = new Inventario();
        assertNull(inventario.buscar("Malha de ferro"));
    }

    @Test
    public void buscarItemComMesmaDescricaoRetornaPrimeiro() {
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada de aço valiriano", 3);
        Item espada2 = new Item("Espada de aço valiriano", 2);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(espada2);
        Item resultado = inventario.buscar("Espada de aço valiriano");
        assertEquals("Espada de aço valiriano", resultado.getDescricao());
        assertEquals(3, resultado.getQuantidade());
    }

    @Test
    public void buscarItemForaDoInventarioRetornaNull() {
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada de aço valiriano", 3);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        Item resultado = inventario.buscar("Botas de couro");
        assertNull(resultado);
    }

    @Test
    public void buscarItemNormal() {
        Inventario inventario = new Inventario();
        Item lanca = new Item("Lança", 1);
        Item espada = new Item("Espada de aço valiriano", 3);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        Item resultado = inventario.buscar("Lança");
        assertEquals("Lança", resultado.getDescricao());
        assertEquals(1, resultado.getQuantidade());
    }

    @Test
    public void inverterInventarioVazio() {
        Inventario inventario = new Inventario();
        assertTrue(inventario.inverter().isEmpty());
    }

    @Test
    public void inverterInventarioComApenasUmElemento() {
        Inventario inventario = new Inventario();
        Item bracelete = new Item("Bracelete prateado", 2);
        inventario.adicionar(bracelete);
        ArrayList<Item> invertidos = inventario.inverter();
        assertEquals(bracelete, invertidos.get(0));
        assertEquals(1, invertidos.size());
    }

    @Test
    public void inverterInventarioComItensIguais() {
        Inventario inventario = new Inventario();
        Item bracelete1 = new Item("Bracelete prateado", 2);
        Item bracelete2 = new Item("Bracelete prateado", 2);
        inventario.adicionar(bracelete1);
        inventario.adicionar(bracelete2);
        ArrayList<Item> invertidos = inventario.inverter();
        assertEquals(bracelete2, invertidos.get(0));
        assertEquals(bracelete1, invertidos.get(1));
        assertEquals(2, invertidos.size());
    }

    @Test
    public void inverterInventarioComItensDescricaoDiferentes() {
        Inventario inventario = new Inventario();
        Item bracelete1 = new Item("Bracelete prateado", 2);
        Item malha = new Item("Malha de couro", 1);
        inventario.adicionar(bracelete1);
        inventario.adicionar(malha);
        ArrayList<Item> invertidos = inventario.inverter();
        assertEquals(malha, invertidos.get(0));
        assertEquals(bracelete1, invertidos.get(1));
        assertEquals(2, invertidos.size());
    }

    @Test
    public void vazioComInventarioVazio() {
        Inventario inventario = new Inventario();
        assertTrue(inventario.vazio());
    }

    @Test
    public void vazioComInventarioCheio() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Espada", 1));
        assertFalse(inventario.vazio());
    }

    @Test
    public void ordenarItensVazio() {
        Inventario inventario = new Inventario();
        inventario.ordenarItens();
        assertTrue(inventario.getItens().isEmpty());
    }

    @Test
    public void ordenarItensUmElemento() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarItensTotalmenteDesordenado() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        Item escudo = new Item("Escudo", 5);
        Item botas = new Item("Botas", 4);
        Item capuz = new Item("Capuz de couro", 4);
        Item armadura = new Item("Armadura de aço", 3);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(botas);
        inventario.adicionar(capuz);
        inventario.adicionar(armadura);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(armadura, botas, capuz, escudo, espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarItensTotalmenteOrdenado() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        Item escudo = new Item("Escudo", 5);
        Item botas = new Item("Botas", 4);
        Item capuz = new Item("Capuz de couro", 4);
        Item armadura = new Item("Armadura de aço", 3);
        inventario.adicionar(armadura);
        inventario.adicionar(botas);
        inventario.adicionar(capuz);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(armadura, botas, capuz, escudo, espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarItensTipoOrdenacaoVazio() {
        Inventario inventario = new Inventario();
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertTrue(inventario.getItens().isEmpty());
    }

    @Test
    public void ordenarItensTipoOrdenacaoUmElemento() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarItensTipoOrdenacaoDescTotalmenteDesordenado() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        Item escudo = new Item("Escudo", 5);
        Item botas = new Item("Botas", 4);
        Item capuz = new Item("Capuz de couro", 4);
        Item armadura = new Item("Armadura de aço", 3);
        inventario.adicionar(armadura);
        inventario.adicionar(capuz);
        inventario.adicionar(botas);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(espada, escudo, capuz, botas, armadura));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarItensTipoOrdenacaoDescTotalmenteOrdenado() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        Item escudo = new Item("Escudo", 5);
        Item botas = new Item("Botas", 4);
        Item capuz = new Item("Capuz de couro", 4);
        Item armadura = new Item("Armadura de aço", 3);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(botas);
        inventario.adicionar(capuz);
        inventario.adicionar(armadura);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(espada, escudo, botas, capuz, armadura));
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordenarItensTipoOrdenacaoAscTotalmenteDesordenado() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 10);
        Item escudo = new Item("Escudo", 5);
        Item botas = new Item("Botas", 4);
        Item capuz = new Item("Capuz de couro", 4);
        Item armadura = new Item("Armadura de aço", 3);
        inventario.adicionar(new Item("Espada", 10));
        inventario.adicionar(escudo);
        inventario.adicionar(botas);
        inventario.adicionar(capuz);
        inventario.adicionar(armadura);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        ArrayList<Item> esperado = new ArrayList(Arrays.asList(armadura, botas, capuz, escudo, espada));
        assertEquals(esperado, inventario.getItens());
    }
}
>>>>>>> master
