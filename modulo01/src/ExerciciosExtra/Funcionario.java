public class Funcionario{
    private String nome;
    private double salarioFixo;
    private double totalVendas;
    public Funcionario(String nome, double salario, double vendas){
        this.nome = nome;
        this.salarioFixo = salario;
        this.totalVendas = vendas;
    }
    public double getLucro(){
        double salarioF = salarioFixo - (salarioFixo*0.1);
        double vendas = totalVendas*0.15;
        vendas -= vendas*0.1;
        return salarioF + vendas;
        }
    }