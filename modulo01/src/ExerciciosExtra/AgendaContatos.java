import java.util.*;
public class AgendaContatos{
    private LinkedHashMap<String, String> contatos = new LinkedHashMap<>();
    public AgendaContatos(){
    }
    public void adicionar(String nome, String numero){
        contatos.put(nome, numero);
    }    
    public String consultar(String nome){
        return contatos.get(nome);
    }
    public String obterContato(String telefone){
        Set<String> chaves = contatos.keySet();
        for (String chave : chaves){
            if(telefone.equals(contatos.get(chave))){
                return chave;
            }
        }
        return null;
    }
    public LinkedHashMap<String,String> getContatos(){
        return this.contatos;
    }
    public String csv(){
        StringBuilder descricoes = new StringBuilder(1024);
        Set<String> chaves = contatos.keySet();
        for (String chave : chaves){
            descricoes.append(chave + "," + contatos.get(chave));
            descricoes.append('\n');
        } 
        return descricoes.toString();
    }
}