import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class NumeroTest
{
    @Test
    public void verificaImpar(){
        Numero numero1 = new Numero(5);
        Numero numero2 = new Numero(27);
        Numero numero3 = new Numero(2018);
        assertEquals(true, numero1.impar());
        assertEquals(true, numero2.impar());
        assertEquals(false, numero3.impar());
    }
    @Test
    public void verificaDivisivel(){
        Numero num1 = new Numero(9);
        Numero num2 = new Numero(3);
        assertEquals(true, num1.verificarSomaDivisivel(1892376));
        assertEquals(false, num2.verificarSomaDivisivel(17));
    }
}
