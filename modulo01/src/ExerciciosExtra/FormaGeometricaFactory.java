
public class FormaGeometricaFactory
{
    public static FormaGeometrica criar(int tipo, int x, int y){
        FormaGeometrica formaGeometrica = null;
        switch(tipo){
            case 1:
                //Criar retangulo
                formaGeometrica = new RetanguloImpl();
                break;
            case 2:
                //Criar quadrado
                 formaGeometrica = new QuadradoImpl();
                 break;
        }
        formaGeometrica.setX(x);
        formaGeometrica.setY(y);
        return formaGeometrica;
    }
}
