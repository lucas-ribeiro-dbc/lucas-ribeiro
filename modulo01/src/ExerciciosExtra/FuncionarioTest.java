

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FuncionarioTest
{
 @Test
 public void verificaLucro(){
     Funcionario func1 = new Funcionario("Capitão América", 500.0, 1000.0);
     Funcionario func2 = new Funcionario("Rogue", 500.0, 7840.50);
     assertEquals(585.0, func1.getLucro(), 0);
     assertEquals(1508.4675, func2.getLucro(), 0);
 }
}
