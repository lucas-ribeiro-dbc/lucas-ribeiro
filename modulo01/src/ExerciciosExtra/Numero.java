public class Numero{
    private int numero;
    public Numero(int numero){
        this.numero = numero;
    }
    public boolean impar(){
        if(this.numero % 2 == 0){
            return false;
        }else{
            return true;
        }
    }
    public boolean verificarSomaDivisivel(int valor){
        String num = String.valueOf(valor);
        int total = 0;
        for(int i = 0; i < num.length(); i++){
            total += Integer.parseInt(String.valueOf(num.charAt(i)));
        }
        if(total % this.numero  == 0 || valor == 0 ){
            return true;
            
        }
        return false;
    }
}