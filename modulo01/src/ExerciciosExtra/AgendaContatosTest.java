

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest
{
    @Test
    public void testarAdicionar(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Legolas", "100");
        assertEquals("100", agenda.consultar("Legolas"));
    }
    @Test
    public void testarObterTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Legolas", "100");
        agenda.adicionar("Pedro", "300");
        agenda.adicionar("Carlinhos", "500");
        agenda.adicionar("Lucas", "200");
        assertEquals("500", agenda.consultar("Carlinhos"));
    }
    @Test
    public void testarObterContato(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Legolas", "100");
        agenda.adicionar("Pedro", "300");
        agenda.adicionar("Carlinhos", "500");
        agenda.adicionar("Lucas", "200");
        assertEquals("Carlinhos", agenda.obterContato("500"));
    }
    @Test
    public void testarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");

        assertEquals("Bernardo,555555\nMithrandir,444444\n", agenda.csv());

    }
}
